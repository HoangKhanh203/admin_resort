/* eslint-disable import/prefer-default-export */
import { getUrl } from 'Common/helper';

export const urlHelper = {
  getUrlHomePage: () => ({
    route: {
      to: '/',
    },
  }),
  getUrlAirTransfer: () => ({
    route: {
      to: '/airport-transfer',
    },
  }),
  getUrlHaveParams: (type, number) => {
    const createParams = {
      type,
      number,
    };
    const paramStrUrl = getUrl(createParams);
    return {
      route: {
        to: `/khanh?${paramStrUrl}`,
      },
    };
  },
};
