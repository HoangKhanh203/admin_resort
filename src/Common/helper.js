/* eslint-disable import/prefer-default-export */
import queryString from 'query-string';

export const getUrl = (paramObject) => {
  let paramStr;
  const _paramObject = { ...paramObject } || {};
  if (Object.keys(_paramObject).length > 0) {
    Object.keys(_paramObject).forEach((item) => {
      if (_paramObject[item] === '') {
        delete _paramObject[item];
      }
    });
    paramStr = queryString.stringify(_paramObject, { sort: false });
  } else {
    paramStr = '';
  }
  return paramStr.replace(/%2C/g, ',');
};

// eslint-disable-next-line consistent-return
export const createConstType = (base, act) =>
  ['REQUEST', 'SUCCESS', 'FAILURE'].reduce((acc, type) => {
    const key = `${act}_${type}`;
    acc[key] = `${base}_${act}_${type}`;
    return acc;
  }, {});

export const _MaxString = (stringInput, value, index) => {
  if (stringInput?.length > index) {
    const indexOfValue = stringInput.indexOf(value, index);
    const res = `${stringInput.substring(0, indexOfValue)}...`;
    return res;
  }
  return stringInput;
};

export const  getBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
};

export const formatVND = (val) =>
  val?.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
