import { combineReducers } from "redux";
import user from "./user";
import app from "./app";
import settings from "./settings";
import tagsView from "./tagsView";
import monitor from "./monitor";
import users from './users.reducers';
import rooms from './rooms.reducers';
import services from './services.reducers';
import vouchers from './vouchers.reducer';
import configFunc from './configFunction';


const RootReducers =  combineReducers({
  user,
  app,
  settings,
  tagsView,
  monitor,
  users,
  rooms,
  services,
  vouchers,
  configFunc
});
export default RootReducers;

