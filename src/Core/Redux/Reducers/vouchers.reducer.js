import { get } from "lodash";
import { VOUCHER_TYPE } from "../ConstTypes";

const initialState = {
  listVoucher: [],
};

const Services = (state = initialState, action) => {
  switch (action.type) {
    case VOUCHER_TYPE.GET_LIST_VOUCHERS_SUCCESS:
      return {
        ...state,
        listVoucher: get(action, "payload.listVoucher", []),
      };
    case VOUCHER_TYPE.EDIT_VOUCHER_SUCCESS:
        return {
          ...state,
          listVoucher: get(action, "payload.listVoucher", []),
        };
    case VOUCHER_TYPE.CREATE_VOUCHER_SUCCESS:
      return {
        ...state,
        listVoucher: [...state.listVoucher, get(action, "payload.newVoucher", {})],
      };
    case VOUCHER_TYPE.DELETE_VOUCHER_SUCCESS:
      return {
        ...state,
        listVoucher: get(action, "payload.listVoucher", []),
      };
    default:
      return state;
  }
};



export default Services;
