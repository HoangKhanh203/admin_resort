import { get } from "lodash";
import { USERS_TYPE } from "../ConstTypes";

const initialState = {
  listUsers: []
};

const Users = (state = initialState, action) => {
  switch (action.type) {
    case USERS_TYPE.GET_LIST_USERS_SUCCESS:
      return {
        ...state,
        listUsers: get(action, "payload.listUsers", []),
      };
    case USERS_TYPE.ADD_USER_SUCCESS:
      return {
        ...state,
        listUsers: [...state.listUsers, get(action, "payload.newUser", {})]
      };
    case USERS_TYPE.EDIT_USER_SUCCESS:
      return {
        ...state,
        listUsers: get(action, "payload.listUsers", []),
      };
    case USERS_TYPE.DELETE_USER_SUCCESS:
      return {
        ...state,
        listUsers: get(action, "payload.listUsers", []),
      };
    default:
      return state;
  }
};

export default Users;
