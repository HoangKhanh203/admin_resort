import { SOCKETIO_TYPE } from 'Core/Redux/ConstTypes';

const initialState = {
  SocketIO: () => {},
};

const functionConfig = (state = initialState, action) => {
  switch (action.type) {
    case SOCKETIO_TYPE.SOCKETIO_SUCCESS:
      return {
        ...state,
        socketIO: action.payload,
      };

    default:
      return state;
  }
};
export default functionConfig;
