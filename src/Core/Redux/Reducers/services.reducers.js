import { get } from "lodash";
import { SERVICE_TYPE } from "../ConstTypes";

const initialState = {
  listServices: [],
  listBooking: [],
  listNumberRoom: []
};

const Services = (state = initialState, action) => {
  switch (action.type) {
    case SERVICE_TYPE.GET_LIST_SERVICES_SUCCESS:
      return {
        ...state,
        listServices: get(action, "payload.listServices", []),
      };
    case SERVICE_TYPE.EDIT_SERVICE_SUCCESS:
        return {
          ...state,
          listServices: get(action, "payload.listServices", []),
        };
    case SERVICE_TYPE.ADD_SERVICE_SUCCESS:
      return {
        ...state,
        listServices: [...state.listServices, get(action, "payload.newService", {})],
      };
    case SERVICE_TYPE.DELETE_SERVICE_SUCCESS:
      return {
        ...state,
        listServices: get(action, "payload.listServices", []),
      };
    default:
      return state;
  }
};



export default Services;
