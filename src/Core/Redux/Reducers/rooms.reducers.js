import { get } from "lodash";
import { ROOMS_TYPE } from "../ConstTypes";

const initialState = {
  listRooms: [],
  listBooking: [],
  listNumberRoom: [],
  listPayment: [],

};

const Rooms = (state = initialState, action) => {
  switch (action.type) {
    case ROOMS_TYPE.GET_LIST_ROOMS_SUCCESS:
      return {
        ...state,
        listRooms: get(action, "payload.listRooms", []),
      };
    case ROOMS_TYPE.EDIT_ROOM_SUCCESS:
      return {
        ...state,
        listRooms: get(action, "payload.listRooms", []),
      };
    case ROOMS_TYPE.ADD_ROOM_SUCCESS:
      return {
        ...state,
        listRooms: [...state.listRooms, get(action, "payload.newRoom", {})],
      };
    case ROOMS_TYPE.DELETE_ROOM_SUCCESS:
      return {
        ...state,
        listRooms: get(action, "payload.listRooms", []),
      };

    case ROOMS_TYPE.GET_LIST_BOOKING_SUCCESS:
      return {
        ...state,
        listBooking: get(action, "payload.listBooking", []),
      };
    case ROOMS_TYPE.GET_LIST_NUMBER_ROOM_SUCCESS:
      return {
        ...state,
        listNumberRoom: get(action, "payload.listNumberRoom", []),

      }

    case ROOMS_TYPE.GET_LIST_PAYMENT_SUCCESS:
      return {
        ...state,
        listPayment: get(action, "payload.listPayment", []),
      }
    default:
      return state;
  }
};



export default Rooms;
