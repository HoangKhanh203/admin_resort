import { login, logout } from "./auth";
import { getUserInfo, setUserToken, setUserInfo, resetUser } from "./user";
import { toggleSiderBar, toggleSettingPanel } from "./app";
import { changeSetting } from "./settings";
import { addTag, emptyTaglist, deleteTag, closeOtherTags } from "./tagsView";
import { addBug } from "./monitor";
import * as UsersActions from './users.actions';
import * as RoomsActions from './rooms.actions';
import * as ServicesActions from './services.actions';
import * as VouchersActions from './vouchers.action';
import * as ConfigFunction from './configFunc';

export {
  login,
  logout,
  getUserInfo,
  setUserToken,
  setUserInfo,
  resetUser,
  toggleSiderBar,
  toggleSettingPanel,
  changeSetting,
  addTag,
  emptyTaglist,
  deleteTag,
  closeOtherTags,
  addBug,
  UsersActions,
  RoomsActions,
  ServicesActions,
  VouchersActions,
  ConfigFunction
};
