/* eslint-disable import/prefer-default-export */
import { SOCKETIO_TYPE } from 'Core/Redux/ConstTypes';

export const SocketIO = (func) => (dispatch) => {
  dispatch({
    type: SOCKETIO_TYPE.SOCKETIO_SUCCESS,
    payload: func,
  });
};
