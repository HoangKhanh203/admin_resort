
import { USERS_TYPE } from "../ConstTypes";
import _lodash from "lodash";
import CallApi from 'Core/Api';

export const getListUsers = () => (dispatch) => {
  CallApi.UsersAPI.getListUsers().then(res => {
    const listUsers = res?.data?.data || null;
    if (!listUsers) return null;
    return dispatch({
      type: USERS_TYPE.GET_LIST_USERS_SUCCESS,
      payload: {
        listUsers: listUsers,
      },
    });
  });


};
export const addInfoUser = (_obj) => (dispatch) => {
  const _data = _lodash.get(_obj,'data', {});

  CallApi.UsersAPI.addUser({
    data: _data
  }).then(res => {
    const getStatus = _lodash.get(res, 'data.status', 'ERROR');
    if (getStatus === 'ERROR') return;
    dispatch({
      type: USERS_TYPE.ADD_USER_SUCCESS,
      payload: {
        newUser: _data
      },
    });
    return getStatus
  })

};
export const editInfoUser = (_obj) => dispatch => {
  const dataUser = _lodash.get(_obj, 'dataUser', {});
  return CallApi.UsersAPI.editUser({
    data: dataUser
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const posIndexChange = arrData.findIndex(item => item.id_user === dataUser.id_user);
    arrData[posIndexChange] = { ...dataUser };
    console.log(arrData);
    return dispatch({
      type: USERS_TYPE.EDIT_USER_SUCCESS,
      payload: {
        listUsers: [...arrData]
      }
    });
  })

}
export const deleteInfoUser = (_obj) => dispatch => {
  const dataUser = _lodash.get(_obj,'dataUser', {});
  const id_user = _lodash.get(dataUser, 'info_user.id_user', '');
  const email = _lodash.get(dataUser, 'email', '');

  return CallApi.UsersAPI.deleteUser({
    data: {
      id_user: id_user,
      email: email
    }
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const indexUser = arrData.findIndex(item => res.id_user === id_user);
    arrData.splice(indexUser, 1);

    dispatch({
      type: USERS_TYPE.DELETE_USER_SUCCESS,
      payload: {
        listUsers: arrData
      }
    });
  })

}