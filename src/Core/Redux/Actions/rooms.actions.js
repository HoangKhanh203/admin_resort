
import { ROOMS_TYPE } from "../ConstTypes";
import _lodash from "lodash";
import CallApi from 'Core/Api';

export const getListRooms = () => (dispatch) => {
  CallApi.RoomsAPI.getListRooms().then(res => {
    const listRooms = _lodash.get(res,"data.data", []);
    if (!listRooms) return null;
    return dispatch({
      type: ROOMS_TYPE.GET_LIST_ROOMS_SUCCESS,
      payload: {
        listRooms: listRooms,
      },
    });
  });


};
export const addInfoRoom = (_obj) => (dispatch) => {
  const InfoRoom = _lodash.get(_obj,"data", {});

  const detectImage = _lodash.get(InfoRoom,"images", "");
  const detectServiceRoom = _lodash.get(InfoRoom,"service_room", "");


  InfoRoom.images = [...detectImage];
  InfoRoom.service_room = [...detectServiceRoom];

  return CallApi.RoomsAPI.createRoom({
    data: InfoRoom
  }).then(res => {
    const getStatus = _lodash.get(res, 'data.status', 'ERROR');
    if (getStatus === 'ERROR') return;
    dispatch({
      type: ROOMS_TYPE.ADD_ROOM_SUCCESS,
      payload: {
        newRoom: InfoRoom
      },
    });
    return res.data.status
  })

};
export const editInfoRoom = (_obj) => dispatch => {
  const dataRow = _lodash.get(_obj, 'dataRow', {});
  return CallApi.RoomsAPI.editRoom({
    data: dataRow
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const posIndexChange = arrData.findIndex(item => item.id_user === dataRow.id_user);
    arrData[posIndexChange] = { ...dataRow };
    console.log(arrData);
    return dispatch({
      type: ROOMS_TYPE.EDIT_ROOM_SUCCESS,
      payload: {
        listRooms: [...arrData]
      }
    });
  })

}
export const deleteInfoRoom = (_obj) => dispatch => {
  const dataRow = _lodash.get(_obj,'dataRow', {});

  const id_manager_room = _lodash.get(dataRow, 'id_manager_room', '');

  return CallApi.RoomsAPI.deleteRoom({
    data: {
      id_manager_room: id_manager_room,
    }
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const indexUser = arrData.findIndex(item => item.id_manager_room === id_manager_room);
    arrData.splice(indexUser, 1);
    dispatch({
      type: ROOMS_TYPE.DELETE_ROOM_SUCCESS,
      payload: {
        listRooms: arrData
      }
    });
  })
}
export const getListBooking  = () => dispatch => {
  return CallApi.RoomsAPI.getListBooking().then(res => {
      const data = res.data;
      dispatch({
        type: ROOMS_TYPE.GET_LIST_BOOKING_SUCCESS,
        payload: {
          listBooking: data.data
        }
      });
  })
}

export const checkin = ({
  dataRow,
  arrData
}) => dispatch => {
  return CallApi.RoomsAPI.checkin({
    id_detail_booking: dataRow.id_detail_booking,
    id_number_room: dataRow.id_number_room,
  }).then(res => {
    const _dataAll = [...arrData] || [];
    const posBooking = _dataAll.findIndex(item => item.id_detail_booking === dataRow.id_detail_booking);
    _dataAll[posBooking].status = dataRow.status === 'CHECKIN' ? 'CHECKOUT' : 'CHECKIN';

    dispatch({
      type: ROOMS_TYPE.GET_LIST_BOOKING_SUCCESS,
      payload: {
        listBooking: _dataAll
      }
    });
    return res.data
  })
}

export const checkout = ({
  dataRow,
  arrData
}) => dispatch => {
  return CallApi.RoomsAPI.checkout({
    ...dataRow
  }).then(res => {

    return res.data
  })
}
export const getListNumberRoom = () => dispatch => {
  return CallApi.RoomsAPI.GET_LIST_NUMBER_ROOM().then(res => {
    const listNumberRoom = _lodash.get(res,"data.data", []);
    dispatch({
      type: ROOMS_TYPE.GET_LIST_NUMBER_ROOM_SUCCESS,
      payload: {
        listNumberRoom: listNumberRoom
      }
    })
  })
}

export const changeRoom = ({
  dataRow,
  arrData
}) => dispatch => {
  return CallApi.RoomsAPI.CHANGE_ROOM({
    data: {
      id_detail_booking: dataRow.id_detail_booking,
      id_number_room: dataRow.id_number_room,
    }
  }).then(res => {
    const _dataAll = [...arrData] || [];
    const posBooking = _dataAll.findIndex(item => item.id_detail_booking === dataRow.id_detail_booking);
    _dataAll[posBooking].status = dataRow.status === 'CHECKIN' ? 'CHECKOUT' : 'CHECKIN';

    dispatch({
      type: ROOMS_TYPE.GET_LIST_BOOKING_SUCCESS,
      payload: {
        listBooking: _dataAll
      }
    });
    return res.data
  })
}

export const getListPayment = () => dispatch => CallApi.RoomsAPI.GET_LIST_PAYMENT().then(res => {
  const dataAPI = res.data.data;
  dispatch({  
    type: ROOMS_TYPE.GET_LIST_PAYMENT_SUCCESS,
    payload: {
      listPayment: dataAPI
    }
  })
})