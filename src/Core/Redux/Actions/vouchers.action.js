
import { VOUCHER_TYPE } from "../ConstTypes";
import _lodash from "lodash";
import CallApi from 'Core/Api';

export const getListVoucher = () => (dispatch) => {
  CallApi.VouchersAPI.GET_LIST_VOUCHER().then(res => {
    const listVoucher = _lodash.get(res,"data.data", []);
    if (!listVoucher) return null;
    return dispatch({
      type: VOUCHER_TYPE.GET_LIST_VOUCHERS_SUCCESS,
      payload: {
        listVoucher: listVoucher,
      },
    });
  });

};

export const getListVoucherByID = (_obj) => 
  CallApi.VouchersAPI.GET_LIST_VOUCHER_BY_ID({
    ..._obj
  }).then(res => {
    const voucher = _lodash.get(res,"data.data", []);
    if (!voucher) return null;

    return voucher;
  });
export const createVoucher = (_obj) => (dispatch) => {
  const infoVoucher = _lodash.get(_obj,"data", {});

  return CallApi.VouchersAPI.CREATE_VOUCHER({
    data: infoVoucher
  }).then(res => {
    const getStatus = _lodash.get(res, 'data.status', 'ERROR');
    if (getStatus === 'ERROR') return;
    dispatch({
      type: VOUCHER_TYPE.CREATE_VOUCHER_SUCCESS,
      payload: {
        newVoucher: infoVoucher
      },
    });
    return res.data.status
  })

};
export const editInfoVoucher = (_obj) => dispatch => {
  const dataVoucher = _lodash.get(_obj, 'dataVoucher', {});
  return CallApi.VouchersAPI.EDIT_VOUCHER({
    data: dataVoucher
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const posIndexChange = arrData.findIndex(item => item.id_voucher === dataVoucher.id_voucher);
    arrData[posIndexChange] = { ...dataVoucher };
    return dispatch({
      type: VOUCHER_TYPE.EDIT_VOUCHER_SUCCESS,
      payload: {
        listVoucher: [...arrData]
      }
    });
  })

}
export const deleteInfoVoucher = (_obj) => dispatch => {
  const dataVoucher = _lodash.get(_obj,'dataVoucher', {});

  const id_voucher = _lodash.get(dataVoucher, 'id_voucher', '');

  return CallApi.VouchersAPI.DELETE_VOUCHER({
    data: {
      id_voucher: id_voucher,
    }
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const indexUser = arrData.findIndex(item => item.id_voucher === id_voucher);
    arrData.splice(indexUser, 1);
    dispatch({
      type: VOUCHER_TYPE.DELETE_VOUCHER_SUCCESS,
      payload: {
        listVoucher: arrData
      }
    });
  })
}