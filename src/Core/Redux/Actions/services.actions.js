
import { SERVICE_TYPE } from "../ConstTypes";
import _lodash from "lodash";
import CallApi from 'Core/Api';

export const getListServices = () => (dispatch) => {
  CallApi.ServicesAPI.GET_LIST_SERVICE().then(res => {
    const listServices = _lodash.get(res,"data.data", []);
    if (!listServices) return null;
    return dispatch({
      type: SERVICE_TYPE.GET_LIST_SERVICES_SUCCESS,
      payload: {
        listServices: listServices,
      },
    });
  });


};
export const addInfoService = (_obj) => (dispatch) => {
  const InfoService = _lodash.get(_obj,"data", {});

  return CallApi.ServicesAPI.CREATE_SERVICE({
    data: InfoService
  }).then(res => {
    const getStatus = _lodash.get(res, 'data.status', 'ERROR');
    if (getStatus === 'ERROR') return;
    dispatch({
      type: SERVICE_TYPE.ADD_SERVICE_SUCCESS,
      payload: {
        newService: InfoService
      },
    });
    return res.data.status
  })

};
export const editInfoService = (_obj) => dispatch => {
  const dataRow = _lodash.get(_obj, 'dataRow', {});
  return CallApi.ServicesAPI.EDIT_SERVICE({
    data: dataRow
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const posIndexChange = arrData.findIndex(item => item.id_manager_service === dataRow.id_manager_service);
    arrData[posIndexChange] = { ...dataRow };
    return dispatch({
      type: SERVICE_TYPE.EDIT_SERVICE_SUCCESS,
      payload: {
        listServices: [...arrData]
      }
    });
  })

}
export const deleteInfoService = (_obj) => dispatch => {
  const dataRow = _lodash.get(_obj,'dataRow', {});

  const id_manager_service = _lodash.get(dataRow, 'id_manager_service', '');

  return CallApi.ServicesAPI.DELETE_SERVICE({
    data: {
      id_manager_service: id_manager_service,
    }
  }).then(res => {
    const status = _lodash.get(res, 'data.status', 'ERROR');
    if (status === 'ERROR') return;

    const arrData = _lodash.get(_obj, 'arrData', []);
    const indexUser = arrData.findIndex(item => item.id_manager_service === id_manager_service);
    arrData.splice(indexUser, 1);
    dispatch({
      type: SERVICE_TYPE.DELETE_SERVICE_SUCCESS,
      payload: {
        listServices: arrData
      }
    });
  })
}