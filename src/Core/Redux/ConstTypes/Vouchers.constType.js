import { createConstType } from 'Common/helper'; 

const VOUCHER_TYPE = {
  ...createConstType('VOUCHER', 'CREATE_VOUCHER'),
  ...createConstType('VOUCHER', 'EDIT_VOUCHER'),
  ...createConstType('VOUCHER', 'DELETE_VOUCHER'),
  ...createConstType('VOUCHER', 'GET_LIST_VOUCHERS'),
};
export default VOUCHER_TYPE;
