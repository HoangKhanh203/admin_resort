const createRequestTypes = (base, act) =>
  ['REQUEST', 'SUCCESS', 'FAILURE'].reduce((acc, type) => {
    const key = `${act}_${type}`;
    acc[key] = `${base}_${act}_${type}`;
    return acc;
  }, {});

const NOTINAVICON_TYPE = {
  ...createRequestTypes('SOCKET', 'SOCKETIO'),
};

export default NOTINAVICON_TYPE;
