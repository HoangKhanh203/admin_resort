import { createConstType } from 'Common/helper'; 

const SERVICE_TYPE = {
  ...createConstType('ROOMS', 'ADD_SERVICE'),
  ...createConstType('ROOMS', 'EDIT_SERVICE'),
  ...createConstType('ROOMS', 'DELETE_SERVICE'),
  ...createConstType('ROOMS', 'GET_LIST_SERVICES'),
};
export default SERVICE_TYPE;
