import { createConstType } from 'Common/helper'; 

const ROOMS_TYPE = {
  ...createConstType('ROOMS', 'ADD_ROOM'),
  ...createConstType('ROOMS', 'EDIT_ROOM'),
  ...createConstType('ROOMS', 'DELETE_ROOM'),
  ...createConstType('ROOMS', 'GET_LIST_ROOMS'),
  ...createConstType('ROOMS', 'GET_LIST_BOOKING'),
  ...createConstType('ROOMS', 'GET_LIST_NUMBER_ROOM'),
  ...createConstType('ROOMS', 'GET_LIST_PAYMENT'),


  

};
export default ROOMS_TYPE;
