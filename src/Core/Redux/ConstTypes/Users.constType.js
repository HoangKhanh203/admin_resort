import { createConstType } from 'Common/helper'; 

const USERS_TYPE = {
  ...createConstType('USERS', 'ADD_USER'),
  ...createConstType('USERS', 'EDIT_USER'),
  ...createConstType('USERS', 'DELETE_USER'),
  ...createConstType('USERS', 'GET_LIST_USERS'),

};
export default USERS_TYPE;
