import Constants from "Constants";
import Common from "./Common.api";

const { METHOD_API } = Constants;
const { handleAPI, URL_DOMAIN } = Common;

const CREATE_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/create-service`,
    method: METHOD_API.POST,
    data: _obj.data || {}
});

const EDIT_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/edit-service`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const DELETE_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/delete-service`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const GET_LIST_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/get-list-services`,
    method: METHOD_API.GET
});



export default {
    CREATE_SERVICE,
    EDIT_SERVICE,
    DELETE_SERVICE,
    GET_LIST_SERVICE
}