import RoomsAPI from './Rooms.api';
import UsersAPI from './Users.api';
import ServicesAPI from './Services.api';
import VouchersAPI from './Vouchers.api';

export default {
    RoomsAPI,
    UsersAPI,
    ServicesAPI,
    VouchersAPI
}