
import Constants from "Constants";
import Common from "./Common.api";

const { METHOD_API } = Constants;
const { handleAPI, URL_DOMAIN } = Common;

const createRoom = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/create-room`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const editRoom = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/edit-room`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const deleteRoom = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/delete-room`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const getListRooms = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/get-list-rooms`,
    method: METHOD_API.GET
});

const getListBooking = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/get-list-booking`,
    method: METHOD_API.GET
});

const checkin = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/checkin`,
    method: METHOD_API.POST,
    data: {
        id_number_room: _obj.id_number_room,
        id_detail_booking: _obj.id_detail_booking
    }
});


const checkout = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/checkout`,
    method: METHOD_API.POST,
    data: _obj
})

const GET_LIST_NUMBER_ROOM = async () => await handleAPI({
    url: `${URL_DOMAIN}/get-list-number-room`,
    method: METHOD_API.GET
});





const CREATE_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/create-service`,
    method: METHOD_API.POST,
    data: _obj.data || {}
});

const EDIT_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/edit-service`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const DELETE_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/delete-service`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const GET_LIST_SERVICE = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/get-list-services`,
    method: METHOD_API.GET
});

const CHANGE_ROOM = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/change-room`,
    method: METHOD_API.POST,
    data: _obj.data || {}
});

const GET_LIST_PAYMENT = async () => await handleAPI({
    url: `${URL_DOMAIN}/get-list-payment`,
    method: METHOD_API.GET
});



export default {
    createRoom,
    editRoom,
    deleteRoom,
    getListRooms,
    getListBooking,
    checkin,
    checkout,
    GET_LIST_NUMBER_ROOM,

    CREATE_SERVICE,
    EDIT_SERVICE,
    DELETE_SERVICE,
    GET_LIST_SERVICE,
    CHANGE_ROOM,
    GET_LIST_PAYMENT
}