import _lodash from 'lodash';

import Constants from "Constants";
import Common from "./Common.api";

const { METHOD_API } = Constants;
const { handleAPI, URL_DOMAIN } = Common;

const signIn = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/signin`,
    method: METHOD_API.POST,
    data: _lodash.get(_obj,'data', {} ) 
});

const signUp = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/signup`,
    method: METHOD_API.POST,
    data: _lodash.get(_obj,'data', {} ) 
});

const addUser = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/signup`,
    method: METHOD_API.POST,
    data: _lodash.get(_obj,'data', {} ) 
});

const editUser = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/edit-user`,
    method: METHOD_API.POST,
    data: _lodash.get(_obj,'data', {} ) 
});

const deleteUser = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/delete-user`,
    method: METHOD_API.POST,
    data: _lodash.get(_obj,'data', {} ) 
});

const getListUsers = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/get-list-users`,
    method: METHOD_API.GET,
    data: _lodash.get(_obj,'data', {} ) 
});


export default {
    signIn,
    signUp,
    editUser,
    deleteUser,
    getListUsers,
    addUser
}