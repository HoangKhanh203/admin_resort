import Axios from 'axios';
import _lodash from 'lodash';

import Constants from 'Constants';

const { METHOD_API } = Constants; 
const URL_DOMAIN = "http://localhost:5000";

const handleAPI = (_obj) => {
    const _METHOD = _lodash.get(_obj, 'method', METHOD_API.GET);
    const _URL = _lodash.get(_obj, 'url', '');
    const _data = _lodash.get(_obj, 'data', {});

    return Axios({
        method: _METHOD,
        url: _URL,
        data: _data
    })
    .then(res => res)
    .catch(err => err)

}
export default {
    handleAPI,
    URL_DOMAIN
}