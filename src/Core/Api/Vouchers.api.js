
import Constants from "Constants";
import Common from "./Common.api";

const { METHOD_API } = Constants;
const { handleAPI, URL_DOMAIN } = Common;


const CREATE_VOUCHER = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/create-voucher`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const EDIT_VOUCHER = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/edit-voucher`,
    method: METHOD_API.POST,
    data: _obj.data || {}
    
});

const DELETE_VOUCHER = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/delete-voucher`,
    method: METHOD_API.POST,
    data: _obj.data || {}
});

const GET_LIST_VOUCHER = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/get-list-voucher`,
    method: METHOD_API.GET,
});

const GET_LIST_VOUCHER_BY_ID = async (_obj) => await handleAPI({
    url: `${URL_DOMAIN}/get-voucher-by-id/${_obj.data.id_voucher}`,
    method: METHOD_API.GET,
});

export default {
    CREATE_VOUCHER,
    EDIT_VOUCHER,
    DELETE_VOUCHER,
    GET_LIST_VOUCHER,
    GET_LIST_VOUCHER_BY_ID
}