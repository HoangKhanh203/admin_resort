import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "antd/dist/antd.css";
import "@/styles/index.scss";
import "./mock";
import '@/lib/monitor';
import './assets/Styles/main.scss';

ReactDOM.render(<App />, document.getElementById("root"));
