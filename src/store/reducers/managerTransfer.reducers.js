import { get } from "lodash";
import { MANAGER_TRANSFER_TYPE } from "../ConstTypes";

const initialState = {
  listTransfer: [],
};

const ManagerTransfer = (state = initialState, action) => {
  switch (action.type) {
    case MANAGER_TRANSFER_TYPE.UPDATE_LIST_TRANSFER_SUCCESS:
      return {
        ...state,
        listTransfer: get(action, "payload.listTransfer", []),
      };
    case MANAGER_TRANSFER_TYPE.ADD_LIST_TRANSFER_SUCCESS:
        return {
          ...state,
          listTransfer: [...state.listTransfer, get(action, "payload.info",{})],
        };
    case MANAGER_TRANSFER_TYPE.DELETE_LIST_TRANSFER_SUCCESS:
      return {
        ...state,
        listTransfer: get(action, "payload.listTransfer", []),
      };
    case MANAGER_TRANSFER_TYPE.EDIT_LIST_TRANSFER_SUCCESS:
      return {
        ...state,
        listTransfer: get(action, "payload.listTransfer", []),
      };
    default:
      return state;
  }
};

export default ManagerTransfer;
