import { createConstType } from 'Common/helper'; 

const MANAGER_TRANSFER_TYPE = {
  ...createConstType('MANAGER_TRANSFER', 'UPDATE_LIST_TRANSFER'),
  ...createConstType('MANAGER_TRANSFER', 'DELETE_LIST_TRANSFER'),
  ...createConstType('MANAGER_TRANSFER', 'EDIT_LIST_TRANSFER'),
  ...createConstType('MANAGER_TRANSFER', 'ADD_LIST_TRANSFER'),



};
export default MANAGER_TRANSFER_TYPE;
