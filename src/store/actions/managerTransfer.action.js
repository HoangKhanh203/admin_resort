import { MANAGER_TRANSFER_TYPE } from "../ConstTypes";
import MockData from "MockData";
import { get } from "lodash";

export const getListTransfer = () => (dispatch) => {
  const { listTransfer } = MockData.transfer.data;
  return dispatch({
    type: MANAGER_TRANSFER_TYPE.UPDATE_LIST_TRANSFER_SUCCESS,
    payload: {
      listTransfer: listTransfer,
    },
  });
};
export const addInfoTransfer = (_obj) => (dispatch) => {
  const _data = get(_obj,'info', []);
  return dispatch({
    type: MANAGER_TRANSFER_TYPE.ADD_LIST_TRANSFER_SUCCESS,
    payload: {
      infoTransfer: {..._data},
    },
  });
};
export const deleteInfoTransfer = (_obj) => dispatch => {
  const _data = get(_obj,'data', []);
  if(_data.length < 0) return;

  dispatch({
    type: MANAGER_TRANSFER_TYPE.DELETE_LIST_TRANSFER_SUCCESS,
    payload: {
      listTransfer: [..._data]
    }
  });
  return {
    status: "OK"
  }
}
export const editInfoTransfer = (_obj) => dispatch => {
  const _data = get(_obj,'data', []);
  if(_data.length < 0) return;

   dispatch({
    type: MANAGER_TRANSFER_TYPE.EDIT_LIST_TRANSFER_SUCCESS,
    payload: {
      listTransfer: [..._data]
    }
    });
  return {
    status: "OK"
  }
}