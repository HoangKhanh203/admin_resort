import React, { Component, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from 'react-redux';

import { Card, Button, Table, message, Divider, Select, Input } from "antd";
import { deleteUser, addInfoUser } from "@/api/user";
import TypingCard from '@/components/TypingCard'
import HandleInfoUser from "./forms/add-user-form";
import _lodash from 'lodash';


import * as Actions from 'Core/Redux/Actions';


const { Column } = Table;
const User = () => {
  const dispatch = useDispatch();
  const { listUsers } = useSelector(state => state.users);
  const [listUsersState, setListUsersState] = useState(listUsers);
  const [editUserModalVisible, setEditUserModalVisible] = useState(false);
  const [addUserModalVisible, setAddUserModalVisible] = useState(false);
  const [currentRowData, setCurrentRowData] = useState({});
  const [dataFilter, setDataFilter] = useState(null);
  useEffect(() => {
    dispatch(Actions.UsersActions.getListUsers());
  }, [dispatch])
  useEffect(() => {
    return setListUsersState(listUsers);
  }, [listUsers])

  const handleSetValueDataFilter = (_obj) => {
    const key = _lodash.get(_obj, 'key', '');
    const value = _lodash.get(_obj, 'value', 'null');
    const _dataFilter = { ...dataFilter } || {};
    _dataFilter[key] = value;

    const arrDataFilter = Object.keys(_dataFilter);
    if (value === 'null') {
      delete _dataFilter[key]
    }
    
    if (!value || arrDataFilter.length <= 0 || !key) {
      setListUsersState([...listUsers]);
      return setDataFilter(null);
    }
    let _listDataState = [...listUsers] || [];

    Object.keys(_dataFilter).forEach(itemFilter => {

      _listDataState = _listDataState.filter(item => {
        return RegExp(_dataFilter[itemFilter].toString().toUpperCase(), 'g').exec(_lodash.get(item, itemFilter, 'null').toString().toUpperCase())
      })
    })


    setDataFilter(_dataFilter)
    setListUsersState(_listDataState)
  }


  const arrInputValue = [
    {
      title: "User ID",
      dataIndex: 'info_user.id_user',
      key: "id_user",
      customs: {
        initialValue: _lodash.get(currentRowData, 'info_user.id_user', ''),
        disabled: true
      },
      width: 100
    },
    {
      title: () => (
        <Input placeholder="Email" onChange={(e) => handleSetValueDataFilter({
          key: 'email',
          value: e.target.value
        })}  />
      ),
      // title: "Email",
      dataIndex: 'email',
      key: "email",
      customs: {
        initialValue: _lodash.get(currentRowData, 'email', ''),
      }
    },
    {
      title: "Password",
      dataIndex: 'password',
      key: "password",
      customs: {
        initialValue: _lodash.get(currentRowData, 'password', ''),
      }
    },
    {
      title: () => (
        <Select
          defaultValue="Role"
          style={{
            minWidth: 80
          }}
          onChange={(e) => handleSetValueDataFilter({
            key: 'role',
            value: e
          })}
        >
          <Select.Option key={null}>default</Select.Option>
          <Select.Option key="NORMAL">Normal</Select.Option>
          <Select.Option key="CUSTOMER">Customer</Select.Option>

        </Select>
      ),
      // title: "Role",
      dataIndex: 'info_user.role',
      key: "role",
      customs: {
        type: 'SELECT',
        initialValue: "NORMAL",
        options: [
          {
            value: 'NORMAL',
            label: "Normal",
          },
          {
            value: 'CUSTOMER',
            label: "Customer",
          },
        ],
      }
    },
    {
      title: "Fullname",
      dataIndex: 'info_user.fullname',
      key: "fullname",
      customs: {
        initialValue: _lodash.get(currentRowData, 'info_user.fullname', ''),
      },
    },
    // {
    //   title: "Identify",
    //   dataIndex: 'identify',
    //   key: "identify",
    //   customs: {
    //     initialValue: _lodash.get(currentRowData, 'identify', ''),
    //   }
    // },
    {
      title: () => (
        <Input placeholder="Phone" onChange={(e) => handleSetValueDataFilter({
          key: 'phone_number',
          value: e.target.value
        })}  />
      ),
      // title: "PhoneNumber",
      dataIndex: 'info_user.phone_number',
      key: "phone_number",
      customs: {
        initialValue: _lodash.get(currentRowData, 'phone_number', ''),
      },
      width: 120
    },
  ];


  const OnModalEditUser = (row) => {
    setCurrentRowData(Object.assign({}, row));
    setEditUserModalVisible(true);
  };

  const handleDeleteUser = (row) => {
    // const { id_user } = row
    // if (id === "admin") {
    //   message.error("The administrator user cannot be deleted! ")
    //   return
    // }
    // deleteUser({ id }).then(res => {
    //   message.success("Successfully deleted")
    // })

    dispatch(Actions.UsersActions.deleteInfoUser({
      dataUser: row,
      arrData: listUsersState
    }));
  }

  const handleEditUserOk = (_data) => {
    dispatch(Actions.UsersActions.editInfoUser({
      dataUser: _data,
      arrData: listUsersState
    }));
    setEditUserModalVisible(false);
    setCurrentRowData({});
    // const { form } = this.editUserFormRef.props;
    // form.validateFields((err, values) => {
    // if (err) {
    //   return;
    // }

    // editUser(values).then((response) => {
    //   form.resetFields();
    //   this.setState({ editUserModalVisible: false, editUserModalLoading: false });
    //   message.success("Edit successfully!")
    //   this.getUsers()
    // }).catch(e => {
    //   message.success("Editing failed, please try again! ")
    // })

    // });
  };

  const handleCancel = _ => {
    setEditUserModalVisible(false);
    setAddUserModalVisible(false);
  };

  const handleAddUser = (row) => {
    setAddUserModalVisible(true);
  };

  const handleAddUserOk = async (_data) => {
    setCurrentRowData({});
    await dispatch(Actions.UsersActions.addInfoUser({
      data: _data
    }));
    setAddUserModalVisible(false);
    setCurrentRowData({});


    // const { form } = this.addUserFormRef.props;
    // form.validateFields((err, values) => {
    //   if (err) {
    //     return;
    //   }
    //   addUser(values).then((response) => {
    //     form.resetFields();
    //     this.setState({ addUserModalVisible: false, addUserModalLoading: false });
    //     message.success("Added successfully!")
    //     this.getUsers()
    //   }).catch(e => {
    //     message.success("Failed to add, please try again!")
    //   })
    // });
  };

  const title = (
    <span>
      <Button type='primary' onClick={handleAddUser}>Add User</Button>
    </span>
  )

  const renderColumnData = () =>
    arrInputValue.map((item) => <Column {...item} />);

  const cardContent = `Here, you can manage users in the system, such as adding a new user, or modifying an existing user in the system。`
  return (
    <div className="app-container">
      <TypingCard title='Group User' source={cardContent} />
      <br />
      <Card title={title} bodyStyle={{
        paddingLeft: 0,
        paddingRight: 0
      }}>
        {listUsersState.length > 0 && (
          <Table bordered dataSource={listUsersState} pagination={false}>
            {renderColumnData()}

            {/*             
            <Column title="User ID" dataIndex="id_user" key="id_user" align="center" defaultFilteredValue=""/>
            <Column title="Email" dataIndex="email" key="email" align="center" defaultFilteredValue=""/>
            <Column title="Role" dataIndex="role" key="role" align="center" defaultFilteredValue=""/>
            <Column title="Fullname" dataIndex="fullname" key="fullname" align="center" defaultFilteredValue=""/>
            <Column title="Identify" dataIndex="identify" key="identify" align="center" defaultFilteredValue=""/>
            <Column title="PhoneNumber" dataIndex="phone_number" key="phone_number" align="center" defaultFilteredValue=""/>
            <Column title="Rooms" dataIndex="rooms" key="rooms" align="center" defaultFilteredValue=""/>
            <Column title="Services" dataIndex="services" key="services" align="center" defaultFilteredValue=""/>
*/}
            <Column title="Action" key="action" render={(text, row) => (
              <span>
                <Button type="primary" shape="circle" icon="edit" title="Edit" onClick={OnModalEditUser.bind(null, row)} />
                <Divider type="vertical" />
                <Button type="primary" shape="circle" icon="delete" title="Delete" onClick={handleDeleteUser.bind(null, row)} />
              </span>
            )} />
          </Table>
        )}
      </Card>

      <HandleInfoUser
        propsModal={{
          title: "Edit"
        }}
        listInput={arrInputValue}
        visible={editUserModalVisible}
        onCancel={handleCancel}
        onOk={handleEditUserOk}
      />
      <HandleInfoUser
        propsModal={{
          title: "Add"
        }}
        listInput={arrInputValue}
        visible={addUserModalVisible}
        onCancel={handleCancel}
        onOk={handleAddUserOk}
      />
    </div>
  );
}

export default User;
