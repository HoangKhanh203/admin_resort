/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react';
import { Switch, Button, Input, Form, Modal } from 'antd';
import * as Helpers from 'Common/helper';
import _lodash from 'lodash';
import './creditcard.scss'
import { useEffect } from 'react';
import { VouchersActions } from 'Core/Redux/Actions';
const CreditCard = (props) => {
  const { dataCard, onCancel, visible, isHaveHotelVSService, handleCheckout } = props;
  const { getFieldDecorator, validateFields } = props.form;
  const [ShowVoucher, setShowVoucher] = useState(false);
  const [voucherPercent, setVoucherPercent] = useState(1);
  const [couponCode, setCouponCode] = useState(0);
  const [pricePromotion, setPricePromotion] = useState(0);
  const [total_money, setTotalMoney] = useState(0);
  const [payDirect, setPayDirect] = useState(true);

  useEffect(() => {
    let discount = 0;
    const price = dataCard.reduce((pre, value) => {
      if (isHaveHotelVSService && value.type === 'SERVICE') {
        discount += value.payment.total_money / 2;
      }
      return pre + value?.payment?.total_money - value?.payment?.deposit_money
    }, 0);
    setPricePromotion(discount);
    setTotalMoney(isHaveHotelVSService ? price - discount : price);


  }, [dataCard])
  const handleInputCoupon = (e) => {
    setCouponCode(e.target.value);
  };
  const handleCallApiCoupon = async () => {
    const dataVoucher = await VouchersActions.getListVoucherByID({
      data: {
        id_voucher: couponCode
      }
    });
    setVoucherPercent(dataVoucher.percent);
  };

  const handleSubmitPaymentCard = async (e) => {
    const getArrID = dataCard.map(item => item.id_detail_booking);
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
      const data = {
        infoUserPayment: {
          ...values,
        },
        id_voucher: couponCode,
        arrIdDetailBooking: getArrID
      };
      console.log(data);
      handleCheckout({
        data: data
      });
    });


  };

  return (
    <Modal bodyStyle={{
      padding: 0
    }}
      centered
      visible={visible} footer={false} onCancel={onCancel}>
      <div className="credit_card_component">
        <div className="credit_card_component_container">
          <Form id="form_input_credit_card" onSubmit={handleSubmitPaymentCard}>
            <div className="header">
              <div className="header_logo">
                Thanh Toán
              </div>
            </div>
            <div className="wrap_info_credit_card">
              <div className="wrap_info">
                <Form.Item
                  label="Họ tên"
                  className="item_form"
                  name="name"
                >
                  {getFieldDecorator('name')(
                    <Input size="large" placeholder="Tên trên thẻ" />
                  )}

                </Form.Item>
                {payDirect && (
                  <>
                    <Form.Item name="bank" label="Tên Ngân hàng" className="item_form">
                      {getFieldDecorator('bank')(
                        <Input size="large" placeholder="Tên Ngân hàng" />

                      )}
                    </Form.Item>
                    <Form.Item
                      label="Số thẻ tín dụng"
                      name="number_credit"
                      className="item_form"
                    >
                      {getFieldDecorator('number_credit')(
                        <Input size="large" placeholder="16 chữ số trên mặt thẻ" />

                      )}
                    </Form.Item>
                  </>
                )}



              </div>
            </div>
            <div className="wrap_coupon">
              <div className="wrap_btb_switch">
                <Switch
                  size="small"
                  onChange={() => setShowVoucher(!ShowVoucher)}
                />
                <div className="title">Nhập mã giảm giá</div>
              </div>
              <div className="wrap_btb_switch">
                <Switch
                  size="small"
                  onChange={() => setPayDirect(!payDirect)}
                  checked={!payDirect}
                />
                <div className="title">Thanh toán trực tiếp</div>
              </div>
              {ShowVoucher && (
                <div className="wrap_input_coupon">
                  <Input
                    size="large"
                    placeholder="Nhập mã giảm giá"
                    onChange={handleInputCoupon}
                  />
                  <Button
                    type="primary"
                    className="apply_coupon"
                    onClick={handleCallApiCoupon}
                  >
                    Áp dụng mã
                  </Button>
                </div>
              )}
            </div>
          </Form>
        </div>
        <div className="detail_price">
          <div className="detail_price_title">
            <h4>Chi tiết giá</h4>
          </div>
          <div className="detail_price_transfer">
            {dataCard.map(item => {
              const itemData = { ...item } || {};
              const id_card = _lodash.get(itemData, 'id_detail_booking', '');

              const titleService = _lodash.get(
                itemData,
                'infoBooking.manager_service.info_service.title'
              );
              const priceService = _lodash.get(itemData, 'infoBooking.manager_service.info_service.price', 1)
              const isHotel = itemData.type === 'HOTEL';

              return (
                <div className="item_price">
                  <div>
                    {_lodash.get(itemData, 'infoBooking.manager_room.info_room.title', titleService)}
                  </div>
                  <div className="wrap_amount_money">
                    <div>
                      {_lodash.get(itemData, 'infoBooking.number_day_booking', 1)} {isHotel ? 'đêm' : 'buổi'}
                    </div>
                    <div className="icon_x">
                      x
                    </div>
                    <div style={{ marginRight: 8 }}>
                      {_lodash.get(itemData, 'infoBooking.amount_room', _lodash.get(itemData, 'infoBooking.amount_book', 1))}
                      {' '}
                      {isHotel ? 'Phòng' : 'Suất'}
                    </div>
                    <div>
                      {Helpers.formatVND(_lodash.get(itemData, 'infoBooking.manager_room.info_room.price', priceService))}
                    </div>
                  </div>
                </div>
              )
            })}
            <div className="line_horization" />
            {isHaveHotelVSService && (
              <div className="item_price">
                <p>Khuyến mãi đặt cả khách sạn và dịch vụ</p>
                <p className="item_price__text_right">
                  {Helpers.formatVND(-pricePromotion)}
                </p>
              </div>
            )}
            {
              voucherPercent > 1 && (
                <div className="item_price">
                  <p>Voucher</p>
                  <p className="item_price__text_right">
                    -{Helpers.formatVND((total_money * (voucherPercent / 100)))}
                  </p>
                </div>
              )
            }
            {console.log(voucherPercent)}
            <div className="item_price">
              <p>Tổng giá tiền (đã trừ đặt cọc )</p>
              <p className="item_price__text_right">
                {Helpers.formatVND(total_money - (voucherPercent > 1 ? (total_money * (voucherPercent / 100)) : 0))}
              </p>
            </div>
          </div>
        </div>
        <div className="lisence">
          <Button form="form_input_credit_card" htmlType="submit">
            Thanh toán Thẻ tín dụng
          </Button>
        </div>
      </div>
    </Modal>
  );
};
const wrapCreditCard = Form.create({ name: 'CreditCard' })(CreditCard);
export default wrapCreditCard;
