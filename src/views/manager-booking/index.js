import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';

import { Card, Button, Table, Input, Divider, Select, Modal, Checkbox } from "antd";
import TypingCard from '@/components/TypingCard'
import _lodash from 'lodash';
import moment from 'moment';

import CreditCard from './CreditCard';
// import io from "socket.io-client";
import * as Actions from 'Core/Redux/Actions';


const { Column } = Table;
const ManagerRoom = () => {
  const dispatch = useDispatch();
  const { listBooking, listRooms } = useSelector(state => state.rooms);
  const [listBookingState, setlistBookingState] = useState(listBooking);
  const [currentRowData, setCurrentRowData] = useState({});
  const [dataFilter, setDataFilter] = useState(null);
  const [showModalChooseRoom, setShowModalChooseRoom] = useState(false);
  const [dataNumberRoomOfRoom, setDataNumberRoomOfRoom] = useState([]);
  const [idNumberRoom, setIdNumberRoom] = useState('');
  const [visiablePayment, setVisiablePayment] = useState(false);
  const [dataPaymentCheckout, setDataPaynentCheckout] = useState({});
  const [typeBtn, setTypeBtn] = useState('');
  const [arrItemToPayment, setArrItemToPayment ]= useState([]);
  const [arrPosItemToPay, setArrPosItemToPay] = useState([]);
  useEffect(() => {
    dispatch(Actions.RoomsActions.getListBooking());
    dispatch(Actions.RoomsActions.getListRooms());

    
  }, []);



  useEffect(() => {
    return setlistBookingState(listBooking);
  }, [listBooking])

  const handleSetValueDataFilter = (_obj) => {
    const key = _lodash.get(_obj, 'key', '');
    const value = _lodash.get(_obj, 'value', 'null');
    const _dataFilter = { ...dataFilter } || {};
    _dataFilter[key] = value;

    const arrDataFilter = Object.keys(_dataFilter);
    if (value === 'null') {
      delete _dataFilter[key]
    }

    if (!value || arrDataFilter.length <= 0 || !key) {
      setlistBookingState([...listBooking]);
      return setDataFilter(null);
    }
    let _listDataState = [...listBooking] || [];

    Object.keys(_dataFilter).forEach(itemFilter => {

      _listDataState = _listDataState.filter(item => {
        return RegExp(_dataFilter[itemFilter].toString().toUpperCase(), 'g').exec(_lodash.get(item, itemFilter, 'null').toString().toUpperCase())
      })
    })


    setDataFilter(_dataFilter)
    setlistBookingState(_listDataState)
  }

  const handleShowModalChooseRoom = (_dataRow) => {
    console.log("numberRoom: ", {
      _dataRow,
      listBookingState
    });
    if(_dataRow.type !== 'HOTEL') return;
    const numberRoom = listBookingState.find(item => _lodash.get(item,'infoBooking.manager_room.id_manager_room', ' ') === _lodash.get(_dataRow,'infoBooking.manager_room.id_manager_room', ''));

    setDataNumberRoomOfRoom([
      {
        id_detail_booking: _dataRow?.id_detail_booking,
        ...numberRoom.infoBooking.manager_room
      }
    ]);
    setShowModalChooseRoom(!showModalChooseRoom);
  };
  const handleShowAllRoom = () => {
    setShowModalChooseRoom(!showModalChooseRoom);
    setDataNumberRoomOfRoom(listRooms)
  }
  const handleCheckout = async (_obj) => {
    await dispatch(Actions.RoomsActions.checkout({
      dataRow: {
        ..._obj.data
      }
    }))
    setVisiablePayment(!visiablePayment);
    
  }
  const handleCheckinRoom = async (_obj) => {
    const id_detail_booking = _obj.id_detail_booking ||dataNumberRoomOfRoom[0]?.id_detail_booking;
    const dataPostAPI = {
      dataRow: {
        id_detail_booking: id_detail_booking,
        id_number_room: idNumberRoom,
        ...dataNumberRoomOfRoom[0]
      },
      arrData: listBookingState
    }

    let dataCheckin = {};
    if(typeBtn === 'CHECKIN' || _obj?.type === 'CHECKIN'){
      dataCheckin =await dispatch(Actions.RoomsActions.checkin(dataPostAPI))
    }
    else {
      dataCheckin =await dispatch(Actions.RoomsActions.changeRoom(dataPostAPI))
    }
    if (dataCheckin.status === 'SUCCESS') {

      // const _dataNumberRoomOfRoom = [...dataNumberRoomOfRoom] || [];
      // const posBooking = listBookingState.findIndex(item => item.id_detail_booking === id_detail_booking);
      // const dataRoomOld = listBookingState[posBooking].infoBooking.id_checkin.id_number_room;
      // const dataRoomsNew = dataNumberRoomOfRoom.map(item => {
      //     console.log(item);
      // })

      // setIdNumberRoom(idNumberRoom);
    }
  };
  const handleCloseModalListRoom = () => {
    setShowModalChooseRoom(!showModalChooseRoom);
    setDataNumberRoomOfRoom([])
  }  
  console.log(arrPosItemToPay);
  const showModalPayment = (_dataRow) => {
    
    setDataPaynentCheckout([_dataRow]);
    setVisiablePayment(true);
  }
  const handlePayment = () => {
    const getDataTopaymnet = arrPosItemToPay.map((item,index) => {
      if(item){
        return listBookingState[index]
      }
      return false;
    });
    console.log("getDataTopaymnet: ", getDataTopaymnet);
    setDataPaynentCheckout(getDataTopaymnet)
    setVisiablePayment(true);

  }

  const filterInfo = (e) => {
      const value = e.target.value;
      if (!value) {
        return setlistBookingState([...listBooking]);
      }
      let _listDataState = [...listBooking] || [];
  
  
        _listDataState = _listDataState.filter(item => {
          const fullname = item?.user?.fullname ||' ';
          const titleRoom = item.infoBooking?.id_checkin?.id_number_room?.title_number_room ||' ';


          const filterNameUser = RegExp(value.toString().toUpperCase(), 'g').test(fullname.toString().toUpperCase());
          const filterNameNumberRoom = value === titleRoom;
          return filterNameUser || filterNameNumberRoom
        })

      if(_listDataState.length > 0){
        return setlistBookingState(_listDataState)
      }
      else {
        return setlistBookingState([...listBooking]);
      }
  
  }
  const titleCard = () => (
    <>
      <Button type="primary" onClick={handleShowAllRoom} style={{ marginRight: 16 }}>All Room</Button>
      {arrPosItemToPay.length > 0 && (
      <Button type="primary" style={{marginRight: 16}} onClick={handlePayment}>Payment</Button>
      )}    
      <Input style={{ width: '200px', marginLeft: 16 }} placeholder="Name Guest or Number Room" onChange={filterInfo}/>
    </>
  )
  const cardContent = `Here, you can manage users in the system, such as adding a new user, or modifying an existing user in the system。`;
  const formatCurrency = (n, style = 'currency', _currency = 'VND') =>
  n.toLocaleString('it-IT', { style, currency: _currency });

  return (
    <div className="app-container manager_room_pages">
      <TypingCard title='Group User' source={cardContent} />
      <br />
      <Card bodyStyle={{
        paddingLeft: 0,
        paddingRight: 0
      }}
        title={titleCard()}
      >
        {listBookingState.length > 0 && (
          <div className="wrap_layout_render_data">
            <div className="wrap_layout_render_data__container">
              <div className="wrap_layout_render_data__container__header">

              </div>
              <div className="wrap_layout_render_data__container__content">
                <div className="wrap_item_column_data">
                  <span className="column_data"></span>
                  <span className="column_data">ID</span>
                  <span className="column_data">Tên</span>
                  <span className="column_data">Tên Phòng</span>
                  <span className="column_data">Từ</span>
                  <span className="column_data">Đến</span>
                  <span className="column_data">Số phòng</span>
                  <span className="column_data">Tình trạng</span>
                  <span className="column_data">Tổng Tiền</span>
                  <span className="column_data flex-column">Chức năng</span>
                </div>


                {
                  listBookingState.map((item, index) => {
                    const id_card = _lodash.get(item, 'id_detail_booking', '');

                    const titleService = _lodash.get(
                      item,
                      'infoBooking.manager_service.info_service.title'
                    );
                    const dateFromService = _lodash.get(
                      item,
                      'infoBooking.time_booking_checkin',
                      Date()
                    );
                    const dateToService = _lodash.get(
                      item,
                      'infoBooking.time_booking_checkout',
                      Date()
                    );
                    const numberRoom = _lodash.get(item,'infoBooking.id_checkin.id_number_room.title_number_room', '');

                    const status = _lodash.get(item, 'status', 'DEPOSIT');
                    const total_money =formatCurrency(_lodash.get(item, 'payment.total_money', 0));
                    const isHotel = item.type === 'HOTEL';

                    return (
                      <div className="wrap_item_column_data">
                        <span className="column_data"><Checkbox checked={arrPosItemToPay[index]} onChange={() => {
                        if(status !== 'CHECKIN') return;
                         const arrItem = [...arrPosItemToPay] || [];
                         arrItem[index] = !arrItem[index];
                         setArrPosItemToPay(arrItem);
                        }} /></span>
                        <span className="column_data">{id_card}</span>
                        <span className="column_data">{_lodash.get(item,'user.fullname', '')}</span>
                        <span className="column_data">{_lodash.get(item,'infoBooking.manager_room.info_room.title', titleService)}</span>
                        <span className="column_data">{moment(dateFromService).format('DD/MM/yyyy')}</span>
                        <span className="column_data">{moment(dateToService).format('DD/MM/yyyy')}</span>
                        <span className="column_data">{numberRoom}</span>
                        <span className="column_data">{status}</span>
                        <span className="column_data">{total_money}</span>
                        <span className="column_data flex-column">
                            {item.status === 'CANCEL'
                            ? <></> 
                            : _lodash.get(item, 'status', 'DEPOSIT') === 'DEPOSIT' || (_lodash.get(item, 'status', 'DEPOSIT') === 'PAID' && !isHotel)
                              ? <Button className="btn_handle" type="primary" onClick={() => {
                                setTypeBtn("CHECKIN")
                                if(isHotel){
                                  handleShowModalChooseRoom(item)
                                }else {
                                  handleCheckinRoom({
                                    id_detail_booking: id_card,
                                    type: 'CHECKIN'
                                  })
                                }
                                }}>{isHotel ? 'Nhận phòng' : 'Sử dụng'}</Button>
                              : item.status  !== 'CHECKOUT' && isHotel && <Button className="btn_handle" type="primary" onClick={() => showModalPayment(item)}>Thanh Toán</Button>
                            }
                            {item.status === 'CHECKIN' && isHotel && (
                              <Button className="btn_handle" type="primary" onClick={() => {
                                setTypeBtn("CHANGEROOM")
                                console.log("itemL ", item)
                                return handleShowModalChooseRoom(item)
                              }}>Đổi phòng</Button>
                            )}
                            {item.status === 'CHECKOUT' ? isHotel ? 'Đã trả phòng' : 'Đã sử dụng' : ''}
                        </span>

                      </div>

                    )
                  })
                }
              </div>
            </div>
          </div>
        )}
      </Card>
      {console.log("dataNumberRoomOfRoom", dataNumberRoomOfRoom)}
      <Modal visible={showModalChooseRoom} footer={false} onCancel={handleCloseModalListRoom} title="Chọn Phòng cho khách" className="modal_choose_room_checkin">
        <div className="wrap_all_room_follow_type">
          {dataNumberRoomOfRoom.length > 0 && dataNumberRoomOfRoom.map(item => {
            const dataRoom = { ...item } || {};
            const dataBooking = listBookingState.find(item => {
              if(item.type !== 'HOTEL') return;
              if (item.infoBooking.manager_room.id_manager_room === dataRoom.id_number_room) {
                return item;
              }
              return false;
            })
            const getInfoBooking = _lodash.get(dataBooking, 'infoBooking', {});
            const timeCheckin = _lodash.get(getInfoBooking, 'time_booking_checkin', '');
            const timeCheckout = _lodash.get(getInfoBooking, 'time_booking_checkout', '');

            return (
              <div className="wrap_type_room">
                <div className="title">
                  {dataRoom?.info_room?.title}
                </div>
                <div className="content">
                  {dataRoom?.number_room?.map(item => (
                    <div className={`wrap_item_room ${idNumberRoom === item.id_number_room && 'is-choose-room' } ${ item.status === 'RUNNING' && 'disable-room'}`}>
                      <div className={`wrap_item_room__container ${item.status}`} onClick={() => {
                        if (item.status === 'RUNNING') return;
                        // if(typeBtn === 'CHECKIN')
                        setIdNumberRoom(item.id_number_room)
                      }}>
                        <div className="title">
                          Phòng {item.title_number_room}
                        </div>
                        {item.status === 'RUNNING' && (
                          <div className="wrap_time">
                            {timeCheckin} {timeCheckout}
                          </div>
                        )}
                      </div>
                    </div>
                  ))}
                </div>
                {(typeBtn === 'CHECKIN' || typeBtn === 'CHANGEROOM') && (
                    <div className="wrap_btn">
                    <Button type="primary" onClick={handleCheckinRoom}>{typeBtn === 'CHECKIN' ? "Nhận" : "Đổi" } phòng</Button>
                    </div>
                )}
                 
              </div>
            )
          })}
        </div>
      </Modal>
        {visiablePayment && (
          <CreditCard
          handleCheckout={handleCheckout}
          isHaveHotelVSService={dataPaymentCheckout.findIndex(item => _lodash.get(item,'type', '') === 'HOTEL') > -1 && dataPaymentCheckout.findIndex(itemCard => _lodash.get(itemCard,'type') === "SERVICE" ) > -1}
          dataCard={dataPaymentCheckout.filter(item => item)} 
          onCancel={() => setVisiablePayment(!visiablePayment)} 
          visible={visiablePayment} 
          />
        )}
       

    </div>
  );
}

export default ManagerRoom;
