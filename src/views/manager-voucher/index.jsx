import React, { Component, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from 'react-redux';

import { Card, Button, Table, message, Divider, Select, Input } from "antd";
import TypingCard from '@/components/TypingCard'
import HandleInfoVoucher from "./forms/add-user-form";
import _lodash from 'lodash';

import moment from 'moment'
import * as Actions from 'Core/Redux/Actions';


const { Column } = Table;
const Voucher = () => {
  const dispatch = useDispatch();
  const { listVoucher } = useSelector(state => state.vouchers);
  const [listVouchersState, setListVouchersState] = useState(listVoucher);
  const [editVoucherModalVisible, setEditVoucherModalVisible] = useState(false);
  const [addVoucherModalVisible, setAddVoucherModalVisible] = useState(false);
  const [currentRowData, setCurrentRowData] = useState({});
  const [dataFilter, setDataFilter] = useState(null);
  useEffect(() => {
    dispatch(Actions.VouchersActions.getListVoucher());
  }, [dispatch])
  useEffect(() => {
    return setListVouchersState(listVoucher);
  }, [listVoucher])

  const arrInputValue = [
    {
      title: "Voucher ID",
      dataIndex: 'id_voucher',
      key: "id_voucher",
      customs: {
        initialValue: _lodash.get(currentRowData, 'id_voucher', ''),
        disabled: true
      },
      width: 100
    },
    {
      title: "Tình trạng",
      dataIndex: 'status',
      key: "status",
      customs: {
        initialValue: _lodash.get(currentRowData, 'status', ''),
      }
    },
    {
      title: "Phần trăm",
      // title: "Email",
      dataIndex: 'percent',
      key: "percent",
      customs: {
        initialValue: _lodash.get(currentRowData, 'percent', ''),
      }
    },
    {
      title: "Từ ngày",
      // title: "Email",
      dataIndex: 'dateFrom',
      key: "dateFrom",
      customs: {
        type: 'DATE_PICKER',
        initialValue: moment(_lodash.get(currentRowData, 'dateFrom', '')),
      }
    },
    {
      title: "Đến ngày",
      // title: "Email",
      dataIndex: 'dateTo',
      key: "dateTo",
      customs: {
        type: 'DATE_PICKER',
        initialValue: moment(_lodash.get(currentRowData, 'dateTo', '')),
      }
    },
  ];


  const OnModalEditVoucher = (row) => {
    setCurrentRowData(Object.assign({}, row));
    setEditVoucherModalVisible(true);
  };

  const handleDeleteVoucher = (row) => {
    dispatch(Actions.VouchersActions.deleteInfoVoucher({
      dataVoucher: row,
      arrData: listVouchersState
    }));
  }

  const handleEditVoucherOk = (_data) => {
    dispatch(Actions.VouchersActions.editInfoVoucher({
      dataVoucher: _data,
      arrData: listVouchersState
    }));
    setEditVoucherModalVisible(false);
    setCurrentRowData({});
  };

  const handleCancel = _ => {
    setEditVoucherModalVisible(false);
    setAddVoucherModalVisible(false);
  };

  const handleAddVoucher = (row) => {
    setAddVoucherModalVisible(true);
  };

  const handleAddVoucherOk = async (_data) => {
    setCurrentRowData({});
    await dispatch(Actions.VouchersActions.createVoucher({
      data: _data
    }));
    setAddVoucherModalVisible(false);
    setCurrentRowData({});
  };

  const title = (
    <span>
      <Button type='primary' onClick={handleAddVoucher}>Add Voucher</Button>
    </span>
  )

  const renderColumnData = () =>
    arrInputValue.map((item) => <Column {...item} />);

  const cardContent = `Here, you can manage users in the system, such as adding a new user, or modifying an existing user in the system。`
  return (
    <div className="app-container">
      <TypingCard title='Group Voucher' source={cardContent} />
      <br />
      <Card title={title} bodyStyle={{
        paddingLeft: 0,
        paddingRight: 0
      }}>
        {listVouchersState.length > 0 && (
          <Table bordered dataSource={listVouchersState} pagination={false}>
            {renderColumnData()}
            <Column title="Action" key="action" render={(text, row) => (
              <span>
                <Button type="primary" shape="circle" icon="edit" title="Edit" onClick={OnModalEditVoucher.bind(null, row)} />
                <Divider type="vertical" />
                <Button type="primary" shape="circle" icon="delete" title="Delete" onClick={handleDeleteVoucher.bind(null, row)} />
              </span>
            )} />
          </Table>
        )}
      </Card>

      <HandleInfoVoucher
        propsModal={{
          title: "Edit"
        }}
        listInput={arrInputValue}
        visible={editVoucherModalVisible}
        onCancel={handleCancel}
        onOk={handleEditVoucherOk}
      />
      <HandleInfoVoucher
        propsModal={{
          title: "Add"
        }}
        listInput={arrInputValue}
        visible={addVoucherModalVisible}
        onCancel={handleCancel}
        onOk={handleAddVoucherOk}
      />
    </div>
  );
}

export default Voucher;
