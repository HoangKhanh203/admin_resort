import React from "react";
import { Form, Input, Select, Modal, DatePicker } from "antd";
import _lodash from 'lodash';

import './handleInfoUser.scss';

const handleInfoUser = (props) => {

  const { visible, onCancel, onOk,  listInput = [], confirmLoading, propsModal = {
    title: "Add"
  }, form } = props;
  const { getFieldDecorator } = form;

  const _onOK = () => {
    form.validateFields((err, fields) => {
      form.resetFields();
      return onOk(fields);
    })
  }

  const renderTypeElement = (_customs) => {

    const _type = _lodash.get(_customs, 'type', 'INPUT');
    switch (_type) {
      case "IMAGE":
        return <img {..._customs} alt="a" />;
      case "SELECT":
        return (
          <Select>
            {_customs?.options.map((item) => (
              <Select.Option value={item.value}>
                {item.label}
              </Select.Option>
            ))}
          </Select>
        );
      case "TEXTAREA":
        return <Input.TextArea />;
      case "DATE_PICKER":
        return <DatePicker />
      default:
        return (
          <Input
            {..._customs}
          />
        );
    }
  };

  const renderListInput = () =>
    listInput.map((item) => (
      <Form.Item
        label={_lodash.get(item, "title")}
        className="item_form"
        key={_lodash.get(item, "key", "")}
      >
        {getFieldDecorator(_lodash.get(item, "key", ""), {
          rules: [
            {
              required: _lodash.get(item, "required", false),
              message: _lodash.get(item, "messageErr", "Please Input"),
            },
          ],
          initialValue: _lodash.get(item, "customs.initialValue", ""),
        })(renderTypeElement(_lodash.get(item, "customs", {})))}
      </Form.Item>
    ));

  return (
    <Modal
      title={propsModal.title}
      visible={visible}
      onCancel={onCancel}
      onOk={_onOK}
      confirmLoading={confirmLoading}
      bodyStyle={{
        width: '800px'
      }}
    >
      <Form className="form_wrap_input_field">
        {renderListInput()}
      </Form>
    </Modal>
  );
}

export default Form.create({ name: "AddUserForm" })(handleInfoUser);
