import React, { useState, useEffect, useMemo } from "react";
import "./index.less";
import { Row, Select } from 'antd';
import { useDispatch, useSelector } from "react-redux";
import PanelGroup from "./components/PanelGroup";
import * as Actions from "Core/Redux/Actions";
import _lodash from 'lodash';
import BarChart from "./components/BarChart";

const lineChartDefaultData = {
  "New Visits": {
    expectedData: [100, 120, 161, 134, 105, 160, 165],
    actualData: [120, 82, 91, 154, 162, 140, 145],
  },
  Messages: {
    expectedData: [200, 192, 120, 144, 160, 130, 140],
    actualData: [180, 160, 151, 106, 145, 150, 130],
  },
  Purchases: {
    expectedData: [80, 100, 121, 104, 105, 90, 100],
    actualData: [120, 90, 100, 138, 142, 130, 130],
  },
  Shoppings: {
    expectedData: [130, 140, 141, 142, 145, 150, 160],
    actualData: [120, 82, 91, 154, 162, 140, 130],
  },
};

const Dashboard = () => {
  const [lineChartData, setLineChartData] = useState(
    lineChartDefaultData["New Visits"]
  );
  const [dataDashboard, setDataDashboard] = useState([]);

  const dispatch = useDispatch();
  const { listRooms, listBooking, listNumberRoom, listPayment } = useSelector((state) => state.rooms);
  const { listUsers } = useSelector((state) => state.users);
  // const { listBooking } = useSelector((state) => state.services);



  useEffect(() => {
    dispatch(Actions.RoomsActions.getListBooking());
    dispatch(Actions.RoomsActions.getListPayment());
    dispatch(Actions.RoomsActions.getListNumberRoom());
    dispatch(Actions.RoomsActions.getListRooms());
    dispatch(Actions.UsersActions.getListUsers());

  }, [dispatch]);

  useEffect(() => {
    handleDataDashboard();
  }, [listPayment])
  const handleSetLineChartData = (type) => setLineChartData(lineChartDefaultData[type]);

  const handleCountRoomRunning = () => {
    const data = listNumberRoom.reduce((preValue, values) => {
      const findRoomRunning = values.number_room.filter(item => item.status === 'RUNNING');
      return preValue + findRoomRunning.length
    }, 0);

    return data;
  }

  function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    // Get first day of year
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    // Return array of year and week number
    return [d.getUTCFullYear(), weekNo];
  }

  const handleDataDashboard = (type = 'WEEK') => {
    const dataWeek = [...Array(52).keys()]
    const dataMonth = [...Array(12).keys()]

    const dataBookingOfPartner = listPayment.map(item => {
      const date = new Date(_lodash.get(item, 'date_payment', Date()));
      const getWeek = getWeekNumber(date)[1];
      const getMonth = date.getMonth();
      const getYears = date.getFullYear();


      const price = _lodash.get(item, 'total_money', 0);
      dataWeek[getWeek] = price + _lodash.get(dataWeek, `[${getWeek}]`, 0);
      dataMonth[getMonth] = price + _lodash.get(dataMonth, `[${getMonth}]`, 0);

    }).filter(item => item);

    const isWeek = type === 'WEEK';

    const dataToGet = isWeek ? dataWeek : dataMonth;

    setDataDashboard(dataToGet.map((item, index) => ({
      dataBarChat: {
        infoCol: {
          name: "Payment",
          data: item
        }

        ,
        nameCol: `${isWeek ? 'tuần ' : 'tháng '}` + (index + 1)
      }
    })));
  };
  console.log(dataDashboard);

  const renderChart = useMemo(() => (
    <div className="chart-wrapper">
      <BarChart dataChart={dataDashboard.map(item => item?.dataBarChat)} />
    </div>
  ), [dataDashboard])
  return (
    <div className="app-container">

      <PanelGroup handleSetLineChartData={handleSetLineChartData} dataPanelGroup={[
        {
          type: "Totall Booking",
          icon: "user",
          num: listBooking.length,
          color: "#40c9c6",
        },
        {
          type: "Totall Money",
          icon: "user",
          num: listPayment.reduce((preValue, values) => {
            return preValue + values.total_money;
          }, 0),
          color: "#40c9c6",
        },
        {
          type: "Tổng cộng loại phòng",
          icon: "user",
          num: listRooms.length,
          color: "#40c9c6",
        },
      ]} />
     
      {dataDashboard.length > 0 && (
        <Row gutter={32}>
          {/* <Col xs={24} sm={24} lg={8}>
          <div className="chart-wrapper">
            <RaddarChart />
          </div>
        </Col> */}
          {/* <Col xs={24} sm={24} lg={8}>
          <div className="chart-wrapper">
            <PieChart />
          </div>
        </Col> */}
          {/* <Col xs={24} sm={24} lg={8}> */}
          <Select style={{
            width: 200
          }} onSelect={(res) => {
            console.log(res);
            handleDataDashboard(res);
          }}>
            <Select.Option value="WEEK">Tuần</Select.Option>
            <Select.Option value="MONTH">Tháng</Select.Option>

          </Select>
          {renderChart}
          {/* </Col> */}
        </Row>
      )}

    </div>
  );
};

export default Dashboard;
