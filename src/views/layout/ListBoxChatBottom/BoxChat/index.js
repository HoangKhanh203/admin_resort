import React, { useState, useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
// Package
import { Avatar, Input } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import ScrollToBottom from 'react-scroll-to-bottom';

// Function Helper
import _lodash from 'lodash';


import './BoxChat.scss';

const BoxChat = (props) => {
  const { CloseBoxChat, dataMessenger } = props;
  const { arrMessBox = [], username } = dataMessenger;
  const { socketIO } = useSelector((state) => state.configFunc);

  const [messageText, setMessage] = useState(''); // single message


  console.log('socketIO: ', socketIO);

  // function sending messages
  const sendMessage = useCallback(
    (event) => {
      event.preventDefault();
      if (messageText) {
        socketIO.emit('SEND_MESSAGE_CSS', {
          username: username,
          message: messageText,
        }, () => {
          setMessage('');
        });
      }
    },
    [messageText, socketIO]
  );

  const [lengthTextInput, setLengTextInput] = useState(1);
  const onChangeInput = useCallback(
    (e) => {
      const length = Math.ceil(e.target.value.length / 28);
      const Check5line = length > 0 ? length : 1;
      setMessage(e.target.value);
      if (e.target.value === '\n' && Check5line <= 5)
        return setLengTextInput(lengthTextInput + 1);
      if (length !== lengthTextInput && Check5line <= 5) {
        setLengTextInput(Check5line);
      }
    },
    [lengthTextInput]
  );

  const renderItemMessText = useMemo(
    () =>
      arrMessBox.map((mess) => (
        <div className="item-mess-text" key={_lodash.get(mess, 'id', '123456')}>
          <div className={`wrap-mess-author ${mess.userSend && 'user-send'}`}>
            <p className="author">{mess.user_send ? 'Khanh' : 'Ngoc'}</p>
            <p className="content-mess">{_lodash.get(mess, 'text', 'Ngọcccc')}</p>
          </div>
        </div>
      )),
    [arrMessBox]
  );

  return (
    <div className="boxchat-main">
      <div className="boxchat-main__container">
        <div className="title-box-chat" onClick={() => CloseBoxChat()}>
          <div className="wrap-name-close">
            <div className="name-user">nguyen hoang khanh</div>
          </div>
          <CloseOutlined />
        </div>
        {/* <div className="list-text-chat" ref={elementRef}> */}
        <ScrollToBottom className="list-text-chat">
          {renderItemMessText}{' '}
        </ScrollToBottom>
        <div className="input-file">
          <p>
            <Input.TextArea
              className="input-text-chat"
              placeholder="Input Your Mess..."
              rows={lengthTextInput}
              onChange={onChangeInput}
              value={messageText}
              onKeyPress={(event) =>
                event.key === 'Enter' ? sendMessage(event) : null
              }
            />
          </p>
        </div>
      </div>
    </div>
  );
};
export default BoxChat;
BoxChat.propTypes = {
  CloseBoxChat: PropTypes.func,
};
BoxChat.defaultProps = {
  CloseBoxChat: () => {},
};
