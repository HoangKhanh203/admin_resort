import React, { useState, useMemo, useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux'
// Package
import { Avatar } from 'antd';
import { CloseOutlined } from '@ant-design/icons';

// Componenets

import BoxChat from '../BoxChat';

// icon

// color
import './BoxChatSmall.scss';

const BoxChatSmall = () => {
  const { SocketIO } = useSelector(state => state.configFunc);
  const [showBoxChat, setBoxChat] = useState(false);
  const [dataMessenger, setDataMessenger] = useState({});
  useEffect(() => {
      SocketIO.on('SEND_MESSAGE_SSC', (_data) => {
        setDataMessenger(_data);
      });
    return () => {
      SocketIO.emit('disconnect');
      SocketIO.off();
    };
  }, [SocketIO]);

  const CloseBoxChat = useCallback(() => {
    setBoxChat(false);
  }, []);
  const renderBoxChatSmall = useMemo(
    () => (
      <div
        className="boxchatsmall-main"
        onClick={() => {
          setBoxChat(true);
        }}
      >
        <div className="boxchatsmall-main__container">
          <div className="infor-user">
            <div className="name-user">
              <div className="name">Nguyen Hoang Khanh </div>
              <div>
                <CloseOutlined />
              </div>
            </div>
          </div>
        </div>
      </div>
    ),
    []
  );
  return (
    <div className="wrap-boxchat">
      {!showBoxChat && renderBoxChatSmall}
      {showBoxChat && (
        <BoxChat CloseBoxChat={CloseBoxChat} dataMessenger={dataMessenger} />
      )}
    </div>
  );
};
export default BoxChatSmall;
