import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import Content from "./Content";
import Header from "./Header";
import RightPanel from "./RightPanel";
import Sider from "./Sider";
import TagsView from "./TagsView";
import ListBoxChatBottom from './ListBoxChatBottom/BoxChatSmall';
import io from 'socket.io-client';
import { Layout } from "antd";
import { SocketIO } from "Core/Redux/Actions/configFunc";
const Main = (props) => {
  const dispatch = useDispatch();

  useEffect(() => {
    const socket = io.connect('http://localhost:5050?token=' + JSON.stringify({ username: "admin" }), {
      withCredentials: true, transports: ['websocket'], path: '/'
    })
    dispatch(SocketIO(socket));
    // socket.emit('IS_ONLINE_CSS', { username: 'test' })
    // socket.on('SEND_MESSAGE_SSC', data => {
    //   console.log(data)
    // })
    // socket.emit('SEND_MESSAGE_CSS', {username: localStorage.getItem('friend'), message: document.querySelector('input').value})
    // socket.on('UPDATE_LIST_USER_ONLINE_SSC',(props) => {
    //   console.log(props)
    // })

}, []);
  const { tagsView } = props;
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider />
      <Layout>
        <Header />
        {tagsView ? <TagsView /> : null}
        <Content />
        <RightPanel />
        {/* <ListBoxChatBottom /> */}
      </Layout>
    </Layout>
  );
};
export default connect((state) => state.settings)(Main);
