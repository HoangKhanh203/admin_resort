import React, { useState } from 'react';
import { Upload, Modal } from 'antd';
import { getBase64 } from 'Common/helper';
import _lodash from 'lodash';
import { PlusOutlined } from '@ant-design/icons';
import Axios from 'axios';

const UploadImage = (props) => {

    const { listImages } = props;
    const [listImagesState, setListImageState] = useState([...listImages] || []);
    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [previewTitle, setPreviewTitle] = useState('');
    console.log(listImagesState);

    const handleChangUpload = async (e) => {
        if(e.target.files && e.target.files[0]){
            
            const formData = new FormData();

            formData.append('image', e.target.files[0]);
            await Axios({
                method: 'POST',
                url: 'http://localhost:5003/images/save-image',
                data: formData,
                formData: formData,
                headers: {
                    "Content-Type": 'multipart/form-data'
                },
            })
            .then(res => {
                const _listImagesState = [...listImagesState] || [];
                _listImagesState.push(res.data.data.src);
                setListImageState(_listImagesState);
              props.onChange(_listImagesState);
            })
            .catch(err => console.log('err'));
        
            
            // setListImageState(image);
        }
        // return e.target.files[0];
    }
    const handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        setPreviewImage(file.url || file.preview)
        setPreviewVisible(true);
        setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
    };
    return (
        <>
            <input id={_lodash.get(props,'id','')} type="file" onChange={handleChangUpload}/>
            {listImagesState.map(item => {
                return (<img src ={'http://localhost:5003/'+item} alt="photo"/>)
            })}
        </>
    );
}
export default UploadImage;
