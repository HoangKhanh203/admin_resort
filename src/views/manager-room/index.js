import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';

import { Card, Button, Table, Input, Divider, Select } from "antd";
import TypingCard from '@/components/TypingCard'
import ModalInfoUser from "./forms/ModalInfoUser";
import _lodash from 'lodash';


import * as Actions from 'Core/Redux/Actions';


const { Column } = Table;
const ManagerRoom = () => {
  const dispatch = useDispatch();
  const { listRooms } = useSelector(state => state.rooms);
  const { listServices } = useSelector(state => state.services);

  const [listRoomsState, setListRoomsState] = useState(listRooms);
  const [listServicesState, setListServicesState] = useState(listServices);
  const [editRoomModalVisible, setEditRoomModalVisible] = useState(false);
  const [addRoomModalVisible, setAddRoomModalVisible] = useState(false);
  const [currentRowData, setCurrentRowData] = useState({});
  const [dataFilter, setDataFilter] = useState(null);

  useEffect(() => {
    dispatch(Actions.RoomsActions.getListRooms());
    dispatch(Actions.ServicesActions.getListServices());

  }, [dispatch])
  useEffect(() => {
     setListRoomsState(listRooms);
    setListServicesState(listServices)
  }, [listRooms, listServices])
  
  const handleSetValueDataFilter = (_obj) => {
    const key = _lodash.get(_obj, 'key', '');
    const value = _lodash.get(_obj, 'value', 'null');
    const _dataFilter = { ...dataFilter } || {};
    _dataFilter[key] = value;

    const arrDataFilter = Object.keys(_dataFilter);
    if (value === 'null') {
      delete _dataFilter[key]
    }
    
    if (!value || arrDataFilter.length <= 0 || !key) {
      setListRoomsState([...listRooms]);
      return setDataFilter(null);
    }
    let _listDataState = [...listRooms] || [];

    Object.keys(_dataFilter).forEach(itemFilter => {

      _listDataState = _listDataState.filter(item => {
        return RegExp(_dataFilter[itemFilter].toString().toUpperCase(), 'g').exec(_lodash.get(item, itemFilter, 'null').toString().toUpperCase())
      })
    })


    setDataFilter(_dataFilter)
    setListRoomsState(_listDataState.length > 0 ? _listDataState : listRooms);
  }
  const formatCurrency = (n, style = 'currency', _currency = 'VND') =>
  n.toLocaleString('it-IT', { style, currency: _currency });

  const arrInputValue = [
    {
      propsColumn: {
        title: "ID MR",
        dataIndex: 'id_manager_room',
        key: "id_manager_room",
        width: 100,
      },
      propsModal: {
        title: 'ID MR',
        key: "id_manager_room",
      },
      customs: {
        initialValue: _lodash.get(currentRowData, 'id_manager_room', ''),
        disabled: true
      },
    },
    {
      propsColumn: {
        title: () => (
          <Input placeholder="Title" onChange={(e) => handleSetValueDataFilter({
            key: 'info_room.title',
            value: e.target.value
          })}  />
        ),
        dataIndex: 'info_room.title',
        key: "info_room.title",
        width: 300
      },
      propsModal: {
        title: 'Title',
        key: "info_room.title",
      },  
      customs: {
        initialValue: _lodash.get(currentRowData, 'info_room.title', ''),
      },
    },
    {
      render: (value) => formatCurrency(value),
      propsColumn: {
        title: () => (
          <Input placeholder="Price" onChange={(e) => handleSetValueDataFilter({
            key: 'info_room.price',
            value: e.target.value
          })}  />
          
        ),
        dataIndex: 'info_room.price',
        key: "info_room.price",
        width: 150,
      },
      propsModal: {
        title: 'Price',
        key: "info_room.price",
      },
      customs: {
        type: 'InputNumber',
        initialValue: _lodash.get(currentRowData, 'info_room.price', ''),
      }
    },
    {
      propsColumn: {
        title: () => (
          <Input placeholder="Rate" onChange={(e) => handleSetValueDataFilter({
            key: 'info_room.rate',
            value: e.target.value
          })}  />
        ),
        dataIndex: 'info_room.rate',
        width: 100

      },
      propsModal: {
        title: 'Rate',
        key: "info_room.rate",
      },
      customs: {
        initialValue: _lodash.get(currentRowData, 'info_room.rate', ''),
      }
    },
    {
      propsColumn: {
        title: () => (
          <Select
            defaultValue="Status"
            style={{
              minWidth: 80
            }}
            onChange={(e) => handleSetValueDataFilter({
              key: 'info_room.status',
              value: e
            })}
          >
            <Select.Option key={null}>default</Select.Option>
            <Select.Option key="RUNNING">Running</Select.Option>
            <Select.Option key="READY">Ready</Select.Option>
            <Select.Option key="OFF">Off</Select.Option>
          </Select>
        ),
        // title: "info_room.status",
        dataIndex: 'info_room.status',
      },
      propsModal: {
        title: 'Status',
        key: "info_room.status",
      },
      customs: {
        type: 'SELECT',
        initialValue: _lodash.get(currentRowData, 'info_room.status', 'READY'),
        options: [
          {
            value: 'RUNNING',
            label: "Running",
          },
          {
            value: 'READY',
            label: "Ready",
          },
          {
            value: 'OFF',
            label: "Off",
          },
        ],
      }
    },
    {
      propsColumn:{
        title: () => (
          <Select
            defaultValue="Type"
            style={{
              minWidth: 80
            }}
            onChange={(e) => handleSetValueDataFilter({
              key: 'info_room.type_room',
              value: e
            })}
          >
            <Select.Option key={null}>default</Select.Option>
            <Select.Option key="Hoàng gia">Hoàng gia</Select.Option>
            <Select.Option key="Vvip">Vvip</Select.Option>
            <Select.Option key="Thường">Thường</Select.Option>
          </Select>
        ),
        // title: "Type of",
        dataIndex: 'info_room.type_room',
      },
      propsModal: {
        title: 'Type Room',
        key: "info_room.type_room",

      },
      customs: {
        type: 'SELECT',
        initialValue: _lodash.get(currentRowData, 'info_room.type_room', 'Thường'),
        options: [
          {
            value: 'Hoàng gia',
            label: "Hoàng gia",
          },
          {
            value: 'Vvip',
            label: "Vvip",
          },
          {
            value: 'Thường',
            label: "Thường",
          },
          
        ],
      }
    },
    {
      propsColumn:{
        title: () => (
          <Select
            defaultValue="Model"
            style={{
              minWidth: 80
            }}
            onChange={(e) => handleSetValueDataFilter({
              key: 'info_room.type_model',
              value: e
            })}
          >
            <Select.Option key={null}>default</Select.Option>
            <Select.Option key="HOTEL">Hotel</Select.Option>
            <Select.Option key="VILLA">Villa</Select.Option>
          </Select>
        ),
        // title: "model",
        dataIndex: 'info_room.type_model',
      },
      propsModal:{
        title: 'Model',
        key: "info_room.type_model",
      },
      customs: {
        type: 'SELECT',
        initialValue: "HOTEL",
        options: [
          {
            value: 'HOTEL',
            label: "Hotel",
          },
          {
            value: 'VILLA',
            label: "Villa",
          }
        ],
      }
    },
    {
      propsColumn:{
        title: () => (
          <Input placeholder="Guest" onChange={(e) => handleSetValueDataFilter({
            key: 'info_room.amount_guest',
            value: e.target.value
          })}  />
        ),
        dataIndex: 'info_room.amount_guest',
        width: 100

      },
      propsModal: {
        title: 'Guest',
        key: "info_room.amount_guest",
      },
      customs: {
        initialValue: _lodash.get(currentRowData, 'info_room.amount_guest', ''),
      }
    },
    
    {
      propsColumn: {
        title: "description",
        dataIndex: 'info_room.description',
        disabled: true

      },
      propsModal: {
        title: "description",
        key: "info_room.description",

      },
      customs: {
        type: "TEXTAREA",
        initialValue: _lodash.get(currentRowData, 'info_room.description', ''),
      }
    },
    {
      propsColumn:{
        title: "images",
        dataIndex: 'info_room.images',
        disabled: true,
      },
      propsModal: {
        title: "images",
        key: "info_room.images",
        valuePropName: 'fileList',
      },
      customs: {
        type: "UPLOAD",
        initialValue: _lodash.get(currentRowData, 'info_room.images', ''),
      },
      render: (arrImage) =>  arrImage?.map(itemImage => (<img alt="rooms" src={itemImage} />))
    },
    {
      propsColumn: {
        title: "services",
        dataIndex: 'services',
        disabled: true
      },
      propsModal: {
        title: "services",
        key: "services",

      },
      customs: {
        // initialValue: _lodash.get(currentRowData, 'services.id_manager_service', ''),
        type: 'SELECT_MULTIPLE',
        options: listServicesState?.map(item => ({
              value: item.id_manager_service,
              label:  item.info_service.title,
        })),
      },
      render: (arrService) =>  arrService?.map(itemService => (<p>{itemService}</p>))
    }
  ];
  console.log(listServicesState);
  const OpenModalEditRoom = (row) => {
    setCurrentRowData(Object.assign({}, row));
    setEditRoomModalVisible(true);
  };

  const handleDeleteRoom = (row) => {
    // const { id_user } = row
    // if (id === "admin") {
    //   message.error("The administrator user cannot be deleted! ")
    //   return
    // }
    // deleteUser({ id }).then(res => {
    //   message.success("Successfully deleted")
    // })
    dispatch(Actions.RoomsActions.deleteInfoRoom({
      dataRow: row,
      arrData: listRoomsState
    }));
  }

  const handleEditRoomOk = (_data) => {
    dispatch(Actions.RoomsActions.editInfoRoom({
      dataRow: _data,
      arrData: listRoomsState
    }));
    setEditRoomModalVisible(false);
    setCurrentRowData({});

    // const { form } = this.editUserFormRef.props;
    // form.validateFields((err, values) => {
    // if (err) {
    //   return;
    // }

    // editUser(values).then((response) => {
    //   form.resetFields();
    //   this.setState({ editRoomModalVisible: false, editUserModalLoading: false });
    //   message.success("Edit successfully!")
    //   this.getUsers()
    // }).catch(e => {
    //   message.success("Editing failed, please try again! ")
    // })

    // });
  };

  const handleCancel = _ => {
    setEditRoomModalVisible(false);
    setAddRoomModalVisible(false);
    setCurrentRowData({});
  };

  const handleAddRoom = (row) => {
    setAddRoomModalVisible(true);
  };

  const handleAddRoomOk = async (_data) => {
    await dispatch(Actions.RoomsActions.addInfoRoom({
      data: _data
    }));
    const _arrInfoUser = [...listRoomsState] || [];

    _arrInfoUser.push({
      _data
    })
    setListRoomsState(_arrInfoUser);
    setAddRoomModalVisible(false);
    setCurrentRowData({});


    // const { form } = this.addUserFormRef.props;
    // form.validateFields((err, values) => {
    //   if (err) {
    //     return;
    //   }
    //   addUser(values).then((response) => {
    //     form.resetFields();
    //     this.setState({ addRoomModalVisible: false, addUserModalLoading: false });
    //     message.success("Added successfully!")
    //     this.getUsers()
    //   }).catch(e => {
    //     message.success("Failed to add, please try again!")
    //   })
    // });
  };

  const title = (
    <span>
      <Button type='primary' onClick={handleAddRoom}>Add Rooms</Button>
    </span>
  )

  const renderColumnData = () =>
    arrInputValue.map((item) => {

      if(!_lodash.get(item,'propsColumn.disabled', false)){
        return (<Column  className="col-data" {...item.propsColumn || {}} render={item.render || null} />);
      }
      return null;
    });

  const cardContent = `Here, you can manage users in the system, such as adding a new user, or modifying an existing user in the system。`
  return (
    <div className="app-container manager_room_pages">
      <TypingCard title='Group User' source={cardContent} />
      <br />
      <Card title={title} bodyStyle={{
        paddingLeft: 0,
        paddingRight: 0
      }}>
        {listRoomsState.length > 0 && (
          <Table bordered dataSource={listRoomsState} pagination={false}>
            {renderColumnData()}

            <Column title="Action" key="action" align="center" render={(text, row) => (
              <span>
                <Button type="primary" shape="circle" icon="edit" title="Edit" onClick={OpenModalEditRoom.bind(null, row)} />
                <Divider type="vertical" />
                <Button type="primary" shape="circle" icon="delete" title="Delete" onClick={handleDeleteRoom.bind(null, row)} />
              </span>
            )} />
          </Table>
        )}
      </Card>

      <ModalInfoUser
        propsModal={{
          title: "Edit"
        }}
        listInput={arrInputValue}
        visible={editRoomModalVisible}
        onCancel={handleCancel}
        onOk={handleEditRoomOk}
      />
      <ModalInfoUser
        propsModal={{
          title: "Add"
        }}
        listInput={arrInputValue}
        visible={addRoomModalVisible}
        onCancel={handleCancel}
        onOk={handleAddRoomOk}
      />
    </div>
  );
}

export default ManagerRoom;
