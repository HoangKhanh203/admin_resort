import React from "react";
import { Form, Input, Select, Modal, InputNumber } from "antd";

import _lodash from 'lodash';
import './handleInfoUser.scss';
import UploadImage from './handleUpLoadImage';



const HandleInfoUser = (props) => {

  const { visible, onCancel, onOk, listInput = [], confirmLoading, propsModal = {
    title: "Add"
  }, form } = props;
  const { getFieldDecorator } = form;
  const _onOK = () => {
    form.validateFields((err, fields) => {
      form.resetFields();
      return onOk(fields);
    })
  }
  const _onCancel = () => {
    console.log("co")
    form.resetFields();
    onCancel();
  }

  const renderTypeElement = (_customs) => {

    const _type = _lodash.get(_customs, 'type', 'INPUT');
    switch (_type) {
      case "IMAGE":
        return <img {..._customs} alt="a" />;
      case "SELECT":
        return (
          <Select>
            {_customs?.options.map((item) => (
              <Select.Option value={item.value}>
                {item.label}
              </Select.Option>
            ))}
          </Select>
        );
      case 'UPLOAD':
        return (
          <UploadImage listImages={ _lodash.get(_customs, 'initialValue', [])} />
        )
      case "InputNumber":
        return (
          <InputNumber
            style={{
              width: '100%'
            }}
            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            {..._customs}
          />
        )
      case "TEXTAREA":
        return (
          <Input.TextArea rows={5}  />
        )
      default:
        return (
          <Input
            {..._customs}
          />
        );
    }
  };
  const renderListInput = () =>
    listInput.map((item) => {
      const _key = _lodash.get(item, "propsModal.key", "");
      return (
        <Form.Item
          label={_lodash.get(item, "propsModal.title")}
          className="item_form"
          key={_key}
        >
          {getFieldDecorator(_key, {
            // valuePropName: _lodash.get(item,'propsModal.valuePropName', ''),
            rules: [
              {
                required: _lodash.get(item, "propsModal.required", false),
                message: _lodash.get(item, "propsModal.messageErr", "Please Input"),
              },
            ],
            initialValue: _lodash.get(item, "customs.initialValue", ""),
          })(renderTypeElement(_lodash.get(item, "customs", {})))}
        </Form.Item>
      )
    });

  return (
    <Modal
      title={propsModal.title}
      visible={visible}
      onCancel={_onCancel}
      onOk={_onOK}
      confirmLoading={confirmLoading}
      bodyStyle={{
        width: '800px'
      }}
    >
      <Form className="form_wrap_input_field">
        {renderListInput()}
      </Form>
    </Modal>
  );
}

export default Form.create({ name: "AddUserForm" })(HandleInfoUser);
