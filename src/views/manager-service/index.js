import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';

import { Card, Button, Table, Input, Divider, Select } from "antd";
import TypingCard from '@/components/TypingCard'
import ModalInfoUser from "./forms/ModalInfoUser";
import _lodash from 'lodash';


import * as Actions from 'Core/Redux/Actions';


const { Column } = Table;
const ManagerService = () => {
  const dispatch = useDispatch();
  const { listServices } = useSelector(state => state.services);
  const [listServicesState, setlistServicesState] = useState(listServices);
  const [editServiceModalVisible, setEditServiceModalVisible] = useState(false);
  const [addServiceModalVisible, setAddServiceModalVisible] = useState(false);
  const [currentRowData, setCurrentRowData] = useState({});
  const [dataFilter, setDataFilter] = useState(null);

  useEffect(() => {
    dispatch(Actions.ServicesActions.getListServices());
  }, [dispatch])
  useEffect(() => {
    return setlistServicesState(listServices);
  }, [listServices])
  
  const handleSetValueDataFilter = (_obj) => {
    const key = _lodash.get(_obj, 'key', '');
    const value = _lodash.get(_obj, 'value', 'null');
    const _dataFilter = { ...dataFilter } || {};
    _dataFilter[key] = value;

    const arrDataFilter = Object.keys(_dataFilter);
    if (value === 'null') {
      delete _dataFilter[key]
    }
    
    if (!value || arrDataFilter.length <= 0 || !key) {
      setlistServicesState([...listServices]);
      return setDataFilter(null);
    }
    let _listDataState = [...listServices] || [];

    Object.keys(_dataFilter).forEach(itemFilter => {

      _listDataState = _listDataState.filter(item => {
        return RegExp(_dataFilter[itemFilter].toString().toUpperCase(), 'g').exec(_lodash.get(item, itemFilter, 'null').toString().toUpperCase())
      })
    })


    setDataFilter(_dataFilter)
    setlistServicesState(_listDataState.length > 0 ? _listDataState : listServices);
  }
  const formatCurrency = (n, style = 'currency', _currency = 'VND') =>
  n.toLocaleString('it-IT', { style, currency: _currency });

  const arrInputValue = [
    {
      propsColumn: {
        title: "ID Service",
        dataIndex: 'id_manager_service',
        key: "id_manager_service",
        width: 100,
      },
      propsModal: {
        title: 'ID Service',
        key: "id_manager_service",
      },
      customs: {
        initialValue: _lodash.get(currentRowData, 'id_manager_service', ''),
        disabled: true
      },
    },
    {
      propsColumn: {
        title: () => (
          <Input placeholder="Title" onChange={(e) => handleSetValueDataFilter({
            key: 'info_service.title',
            value: e.target.value
          })}  />
        ),
        dataIndex: 'info_service.title',
        key: "info_service.title",
        width: 300
      },
      propsModal: {
        title: 'Title',
        key: "info_service.title",
      },  
      customs: {
        initialValue: _lodash.get(currentRowData, 'info_service.title', ''),
      },
    },
    {
      render: (value) => formatCurrency(value),
      propsColumn: {
        title: () => (
          <Input placeholder="Price" onChange={(e) => handleSetValueDataFilter({
            key: 'info_service.price',
            value: e.target.value
          })}  />
          
        ),
        width: 150,

        // title: "info_service.price",
        dataIndex: 'info_service.price',
        key: "info_service.price",
      },
      propsModal: {
        title: 'Price',
        key: "info_service.price",
      },
      customs: {
        type: 'InputNumber',
        initialValue: _lodash.get(currentRowData, 'info_service.price', ''),
      }
    },
    {
      propsColumn: {
        title: () => (
          <Input placeholder="Rate" onChange={(e) => handleSetValueDataFilter({
            key: 'info_service.rate',
            value: e.target.value
          })}  />
        ),
        dataIndex: 'info_service.rate',
        width: 100,

      },
      propsModal: {
        title: 'Rate',
        key: "info_service.rate",
      },
      customs: {
        initialValue: _lodash.get(currentRowData, 'info_service.rate', ''),
      }
    },
    {
      propsColumn: {
        title: () => (
          <Input placeholder="Total" onChange={(e) => handleSetValueDataFilter({
            key: 'amount_service',
            value: e.target.value
          })}  />
        ),
        dataIndex: 'amount_service',
        width: 200,

      },
      propsModal: {
        title: 'Total',
        key: "amount_service",
      },
      customs: {
        initialValue: _lodash.get(currentRowData, 'amount_service', ''),
      }
    },
    {
      propsColumn: {
        title: () => (
          <Input placeholder="Remain" onChange={(e) => handleSetValueDataFilter({
            key: 'remain_service',
            value: e.target.value
          })}  />
        ),
        dataIndex: 'remain_service',
        width: 200,

      },
      propsModal: {
        title: 'Remain',
        key: "remain_service",
      },
      customs: {
        initialValue: _lodash.get(currentRowData, 'remain_service', ''),
      }
    },
    {
      propsColumn: {
        title: "description",
        dataIndex: 'info_service.description',
        disabled: true

      },
      propsModal: {
        title: "description",
        key: "info_service.description",

      },
      customs: {
        type: "TEXTAREA",
        initialValue: _lodash.get(currentRowData, 'info_service.description', ''),
      }
    },
    {
      propsColumn:{
        title: "images",
        dataIndex: 'info_service.images',
        disabled: true,
      },
      propsModal: {
        title: "images",
        key: "info_service.images",
        valuePropName: 'fileList',
      },
      customs: {
        type: "UPLOAD",
        initialValue: _lodash.get(currentRowData, 'info_service.images', ''),
      },
      render: (arrImage) =>  arrImage?.map(itemImage => (<img alt="rooms" src={itemImage} />))
    },
  ];

  const OpenModalEditService = (row) => {
    setCurrentRowData(Object.assign({}, row));
    setEditServiceModalVisible(true);
  };

  const handleDeleteService = (row) => {
    // const { id_user } = row
    // if (id === "admin") {
    //   message.error("The administrator user cannot be deleted! ")
    //   return
    // }
    // deleteUser({ id }).then(res => {
    //   message.success("Successfully deleted")
    // })
    dispatch(Actions.ServicesActions.deleteInfoService({
      dataRow: row,
      arrData: listServicesState
    }));
  }

  const handleEditServiceOk = (_data) => {
    dispatch(Actions.ServicesActions.editInfoService({
      dataRow: _data,
      arrData: listServicesState
    }));
    setEditServiceModalVisible(false);
    setCurrentRowData({});

    // const { form } = this.editUserFormRef.props;
    // form.validateFields((err, values) => {
    // if (err) {
    //   return;
    // }

    // editUser(values).then((response) => {
    //   form.resetFields();
    //   this.setState({ editServiceModalVisible: false, editUserModalLoading: false });
    //   message.success("Edit successfully!")
    //   this.getUsers()
    // }).catch(e => {
    //   message.success("Editing failed, please try again! ")
    // })

    // });
  };

  const handleCancel = _ => {
    setEditServiceModalVisible(false);
    setAddServiceModalVisible(false);
    setCurrentRowData({});
  };

  const handleAddService = (row) => {
    setAddServiceModalVisible(true);
  };

  const handleAddServiceOk = async (_data) => {
    await dispatch(Actions.ServicesActions.addInfoService({
      data: _data
    }));
    const _arrInfoUser = [...listServicesState] || [];

    _arrInfoUser.push({
      _data
    })
    setlistServicesState(_arrInfoUser);
    setAddServiceModalVisible(false);
    setCurrentRowData({});


    // const { form } = this.addUserFormRef.props;
    // form.validateFields((err, values) => {
    //   if (err) {
    //     return;
    //   }
    //   addUser(values).then((response) => {
    //     form.resetFields();
    //     this.setState({ addServiceModalVisible: false, addUserModalLoading: false });
    //     message.success("Added successfully!")
    //     this.getUsers()
    //   }).catch(e => {
    //     message.success("Failed to add, please try again!")
    //   })
    // });
  };

  const title = (
    <span>
      <Button type='primary' onClick={handleAddService}>Add Services</Button>
    </span>
  )

  const renderColumnData = () =>
    arrInputValue.map((item) => {

      if(!_lodash.get(item,'propsColumn.disabled', false)){
        return (<Column  className="col-data" {...item.propsColumn || {}} render={item.render || null} />);
      }
      return null;
    });

  const cardContent = `Here, you can manage users in the system, such as adding a new user, or modifying an existing user in the system。`
  return (
    <div className="app-container manager_room_pages">
      <TypingCard title='Group User' source={cardContent} />
      <br />
      <Card title={title} bodyStyle={{
        paddingLeft: 0,
        paddingRight: 0
      }}>
        {listServicesState.length > 0 && (
          <Table bordered dataSource={listServicesState} pagination={false}>
            {renderColumnData()}

            <Column title="Action" key="action" align="center" render={(text, row) => (
              <span>
                <Button type="primary" shape="circle" icon="edit" title="Edit" onClick={OpenModalEditService.bind(null, row)} />
                <Divider type="vertical" />
                <Button type="primary" shape="circle" icon="delete" title="Delete" onClick={handleDeleteService.bind(null, row)} />
              </span>
            )} />
          </Table>
        )}
      </Card>

      <ModalInfoUser
        propsModal={{
          title: "Edit"
        }}
        listInput={arrInputValue}
        visible={editServiceModalVisible}
        onCancel={handleCancel}
        onOk={handleEditServiceOk}
      />
      <ModalInfoUser
        propsModal={{
          title: "Add"
        }}
        listInput={arrInputValue}
        visible={addServiceModalVisible}
        onCancel={handleCancel}
        onOk={handleAddServiceOk}
      />
    </div>
  );
}

export default ManagerService;
