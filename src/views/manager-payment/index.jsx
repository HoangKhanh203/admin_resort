import React, { Component, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from 'react-redux';

import { Card, Button, Table, message, Divider, Select, Input } from "antd";
import { deletePayment, addInfoPayment } from "@/api/user";
import TypingCard from '@/components/TypingCard'
import HandleInfoPayment from "./forms/add-user-form";
import _lodash from 'lodash';


import * as Actions from 'Core/Redux/Actions';


const { Column } = Table;
const Payment = () => {
  const dispatch = useDispatch();
  const { listPayment } = useSelector(state => state.rooms);
  const [listPaymentsState, setListPaymentsState] = useState(listPayment);
  const [editPaymentModalVisible, setEditPaymentModalVisible] = useState(false);
  const [addPaymentModalVisible, setAddPaymentModalVisible] = useState(false);
  const [currentRowData, setCurrentRowData] = useState({});
  const [dataFilter, setDataFilter] = useState(null);
  useEffect(() => {
    dispatch(Actions.RoomsActions.getListPayment());
  }, [dispatch])
  useEffect(() => {
    return setListPaymentsState(listPayment);
  }, [listPayment])

  const handleSetValueDataFilter = (_obj) => {
    const key = _lodash.get(_obj, 'key', '');
    const value = _lodash.get(_obj, 'value', 'null');
    const _dataFilter = { ...dataFilter } || {};
    _dataFilter[key] = value;

    const arrDataFilter = Object.keys(_dataFilter);
    if (value === 'null') {
      delete _dataFilter[key]
    }
    
    if (!value || arrDataFilter.length <= 0 || !key) {
      setListPaymentsState([...listPayment]);
      return setDataFilter(null);
    }
    let _listDataState = [...listPayment] || [];

    Object.keys(_dataFilter).forEach(itemFilter => {

      _listDataState = _listDataState.filter(item => {
        return RegExp(_dataFilter[itemFilter].toString().toUpperCase(), 'g').exec(_lodash.get(item, itemFilter, 'null').toString().toUpperCase())
      })
    })


    setDataFilter(_dataFilter)
    setListPaymentsState(_listDataState)
  }

  const formatCurrency = (n, style = 'currency', _currency = 'VND') =>
  n.toLocaleString('it-IT', { style, currency: _currency });

  const arrInputValue = [
    {
      title: "Payment ID",
      dataIndex: 'id_payment',
      key: "id_payment",
      customs: {
        initialValue: _lodash.get(currentRowData, 'id_payment', ''),
        disabled: true
      },
      width: 120
    },
    {
      title: () => (
        <Input placeholder="Name" onChange={(e) => handleSetValueDataFilter({
          key: 'name',
          value: e.target.value
        })}  />
      ),
      // title: "Email",
      dataIndex: 'name',
      key: "name",
      customs: {
        initialValue: _lodash.get(currentRowData, 'name', ''),
      }
    },
    {
      title: "Bank",
      dataIndex: 'bank',
      key: "bank",
      customs: {
        initialValue: _lodash.get(currentRowData, 'bank', ''),
      }
    },
    {
      title: "status",
      dataIndex: 'status',
      key: "status",
    },
    {
      title: "Tổng cộng tiền",
      dataIndex: 'total_money',
      key: "total_money",
      customs: {
        initialValue: _lodash.get(currentRowData, 'total_money', ''),
      },
      render: (value) => formatCurrency(value),
    },
    {
      title: "Ngày thanh toán",
      dataIndex: 'date_payment',
      key: "date_payment",
      customs: {
        initialValue: _lodash.get(currentRowData, 'date_payment', ''),
      }
    },
  ];


  const renderColumnData = () =>
    arrInputValue.map((item) => <Column {...item} />);

  const cardContent = `Here, you can manage users in the system, such as adding a new user, or modifying an existing user in the system。`
  return (
    <div className="app-container">
      <TypingCard title='Group Payment' source={cardContent} />
      <br />
      <Card  bodyStyle={{
        paddingLeft: 0,
        paddingRight: 0
      }}>
        {listPaymentsState.length > 0 && (
          <Table bordered dataSource={listPaymentsState} pagination={false}>
            {renderColumnData()}
          </Table>
        )}
      </Card>
    </div>
  );
}

export default Payment;
