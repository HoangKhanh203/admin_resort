const {
  override,
  fixBabelImports,
  addLessLoader,
  addWebpackAlias,
} = require("customize-cra");
const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}
process.env.CI = "false";

module.exports = override(


  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { "@primary-color": "#1DA57A" },
  }),

  addWebpackAlias({
    "@": resolve("src"),
    Common: resolve('src/Common'),
    MockData: resolve('src/mock/index.mock'),
    Core: resolve('src/Core'),
    Constants: resolve('src/Constants')
  }),
);
